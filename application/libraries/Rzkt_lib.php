<?php

class Rzkt_lib {

	function extract_img_text($text)
	{
		preg_match_all('!(https?:)?//\S+\.(?:jpe?g|jpg|png|gif)!Ui', $text, $matches);
        return $matches[0];
	}

}

/* End of file Rzkt_lib.php */
/* Location: ./application/libraries/Rzkt_lib.php */