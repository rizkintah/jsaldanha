<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontend';
$route['webadmin'] = 'webadmin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['facility'] = "frontend/facility";
$route['facility/detail/(:any)'] = "frontend/get_detail_facility/$1";
$route['news_and_event'] = "frontend/news_and_event";
$route['about_jsu'] = "frontend/about";
$route['schools'] = "frontend/schools";
$route['students'] = "frontend/students";
$route['students/dashboard'] = "frontend/student_dashboard";
$route['schools/(:any)'] = "frontend/school_detail/$1";

$route['staffs'] = "frontend/staffs";
$route['library'] = "frontend/library";
$route['library/(:any)'] = "frontend/get_detail_library/$1";
$route['library/list/(:any)'] = "frontend/get_detail_library_list/$1";
$route['galleries'] = "frontend/galleries";
$route['contact_us'] = "frontend/contact";
$route['news_and_events'] = "frontend/news_and_event";
$route['news_and_events/read/(:any)'] = "frontend/news_and_event_detail/$1";
$route['onlinereg'] = "frontend/onlinereg";
$route['onlinereg_scs'] = "frontend/onlinereg_scs";
$route['submit_online_registration'] = "frontend/submit_online_registration_process";
$route['get_program_faculty/(:any)'] = "frontend/get_list_program_faculty/$1";
$route['get_depart_faculty/(:num)'] = "frontend/get_list_department/$1";
$route['send_mail'] = "frontend/send_mail_proccess";
$route['signout'] = "login/logout_proccess";
$route['admissions'] = "frontend/admissions";
$route['admissions/read/(:any)'] = "frontend/admissions_detail/$1";



# webadmin
$route['webadmin/lectures'] = "webadmin/teachers";
$route['webadmin/lectures/add'] = "webadmin/teachers/add_teacher";
$route['webadmin/lectures/edit/(:any)'] = "webadmin/teachers/edit_teachers/$1";
