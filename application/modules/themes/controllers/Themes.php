<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Themes extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	function showErrorPopOver($alert_type,$textLine){
    	$alert = "<div class='alert ".$alert_type." alert-dismissible fade in' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
              </button>
              ".$textLine."
          </div>";

       return $alert;
  	}

  	function uploaded_files($filename,$upload_path,$newName){
		$this->load->library('upload');
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = '*';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $newName;
		// $config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->upload->initialize($config);

		if (!$this->upload->do_upload($filename))
		{
		    // case - failure
		    $upload_error = array('error' => $this->upload->display_errors());
		}
		else
		{
		    // case - success
		    $upload_data = $this->upload->data();
		    return $upload_data;
		}
    }

    function generateRandomString($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}

}

/* End of file Themes.php */
/* Location: ./application/modules/themes/controllers/Themes.php */