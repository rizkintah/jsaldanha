<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend_model');
		// Modules::run('login/check_isvalidated_student');
		// //Do your magic here
	}

	function index()
	{
		$data['title'] = "Joao Saldanha University";
		$this->load->view('page_home', $data);
	}

	function facility()
	{
		$data['title'] = "Facilities | Joao Saldanha University";
		$data['facilities'] = $this->frontend_model->get_list_main_facilities();
		$this->load->view('page_facility', $data);	
	}

	function get_detail_facility($id)
	{
		$data['title'] = "Detail Facilities | Joao Saldanha University";
		$data['detail'] = $this->frontend_model->get_detail_facility($id);
		$this->load->view('page_detail_facility', $data);	
	}

	function about()
	{
		$data['title'] = "About Us | Joao Saldanha University";
		$data['about'] = $this->frontend_model->get_about()->row_array();
		$this->load->view('page_about', $data);	
	}

	function schools()
	{
		$data['title'] = "Schools | Joao Saldanha University";
		$data['schAv'] = $this->frontend_model->get_faculty_av();
		$data['schpl'] = $this->frontend_model->get_faculty_planning();
		$this->load->view('page_school', $data);	
	}

	function school_detail($title)
	{

		$data['detail'] = $this->frontend_model->get_detail_faculty($title)->row_array();
		$data['title'] = $data['detail']['faculty_name']." | Joao Saldanha University";
		$this->load->view('page_school_detail', $data);
	}

	function staffs()
	{
		$data['title'] = "Staffs | Joao Saldanha University";
		$data['staff'] = $this->frontend_model->get_staff()->row_array();
		$this->load->view('page_staff', $data);		
	}

	function galleries()
	{
		$data['title'] = "Galleries | Joao Saldanha University";
		$data['gal'] = $this->frontend_model->get_galleries();
		$this->load->view('page_gallery', $data);		
		
	}

	function contact()
	{
		$data['title'] = "Contact | Joao Saldanha University";
		$data['contacts'] = $this->frontend_model->get_contact()->row_array();
		$this->load->view('page_contact', $data);		
		
	}

	function send_mail_proccess()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			set_time_limit(0); 
			$firstname 	= $this->input->post('fn');
			$lastName  	= $this->input->post('ln');
			$from 		= $this->input->post('email');
			$msg 		= $this->input->post('msg');
			$mails 		= 'admin@jsaldanha.org';

			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html'; // Append This Line
			$this->email->initialize($config);

			$this->email->from($from, $firstname.' '.$lastName);
			$this->email->subject($sbj);
			$this->email->message($msg);	
			$this->email->to($mails);
			$this->email->send();
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-info','Message successful send'));
			redirect(base_url().'contact_us');
		}else{
			redirect(base_url().'contact_us');
		}
		
	}

	function library()
	{
		$data['title'] = "Library | Joao Saldanha University";
		$data['listFaculty'] = $this->frontend_model->get_list_lib_faculty();
		$this->load->view('page_library', $data);	
	}

	function get_detail_library($id)
	{
		$data['title'] = "Library | Joao Saldanha University";
		$data['listDeptList'] = $this->frontend_model->get_list_lib_dept($id);
		$this->load->view('page_detail_library', $data);	
	}

	function get_detail_library_list($id)
	{
		$data['title'] = "Library | Joao Saldanha University";
		$data['listLibs'] = $this->frontend_model->get_list_lib_book($id);
		$this->load->view('page_detail_list_library', $data);	
	}

	function students()
	{
		if ($this->session->userdata('validated')) {
			redirect(base_url().'students/dashboard');
		}else{
			$data['title'] = "Students | Joao Saldanha University";
			$this->load->view('page_student', $data);	

		}
	}

	function student_dashboard()
	{
		if ($this->session->userdata('validated')) {
			$student_id = $this->session->userdata('ses_student_id');
			$dept_id 	= $this->session->userdata('ses_dept_id');
			$data['compl_unit'] = $this->frontend_model->get_unit_comp($dept_id)->row_array();
			$data['total_unit'] = $this->frontend_model->get_full_unit($dept_id)->row_array();
			$data['stu_detail'] = $this->frontend_model->get_detail_student($student_id)->row_array();
			$data['score_file'] = $this->frontend_model->get_list_score_file($student_id);
			$data['title'] = "Students Dashboard| Joao Saldanha University";
			$this->load->view('page_student_dashboard', $data);
		}else{
			redirect(base_url().'students');
		}
	}

	function news_and_event()
	{
		$data['title'] = "News & Events | Joao Saldanha University";
		$data['listEvent'] = $this->frontend_model->get_list_news_event();
		$this->load->view('page_news_event', $data);	
	}

	function news_and_event_detail()
	{
		$url_en = $this->uri->segment(3);
		$data['detail'] = $this->frontend_model->get_detail_news_event($url_en)->row_array();
		$data['title'] = humanize($url_en, '-')." | Joao Saldanha University";
		$this->load->view('page_news_event_detail', $data);	

	}

}

/* End of file Frontend.php */
/* Location: ./application/modules/frontend/controller/Frontend.php */