<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend_model');
		// Modules::run('login/check_isoauth_valid_student');
		// //Do your magic here
	}

	function index()
	{
		$data['title'] = "Joao Saldanha University";
		$data['about'] = $this->frontend_model->get_about()->row_array();
		$data['listSlide'] = $this->frontend_model->get_slides();
		$this->load->view('page_home', $data);
	}

	function facility()
	{
		$data['title'] = "Facilities | Joao Saldanha University";
		$data['facilities'] = $this->frontend_model->get_facilities();
		$this->load->view('page_facility', $data);	
	}

	function get_detail_facility($id)
	{
		$data['title'] = "Detail Facilities | Joao Saldanha University";
		$data['detail'] = $this->frontend_model->get_detail_facility($id);
		$this->load->view('page_detail_facility', $data);	
	}

	function about()
	{
		$data['title'] = "About Us | Joao Saldanha University";
		$data['about'] = $this->frontend_model->get_about()->row_array();
		$this->load->view('page_about', $data);	
	}

	function schools()
	{
		$data['title'] = "Schools | Joao Saldanha University";
		$data['schAv'] = $this->frontend_model->get_faculty_av();
		$this->load->view('page_school', $data);	
	}

	function school_detail($slug)
	{

		$data['detail'] = $this->frontend_model->get_detail_faculty($slug)->row_array();
		$data['listDepartment'] = $this->frontend_model->get_list_faculty_department($data['detail']['id']);
		$data['title'] = $data['detail']['faculty_name']." | Joao Saldanha University";
		$this->load->view('page_school_detail', $data);
	}

	function staffs()
	{
		$data['title'] = "Staffs | Joao Saldanha University";
		$data['staff'] = $this->frontend_model->get_staff()->row_array();
		$this->load->view('page_staff', $data);		
	}

	function galleries()
	{
		$data['title'] = "Galleries | Joao Saldanha University";
		$data['gal'] = $this->frontend_model->get_galleries();
		$this->load->view('page_gallery', $data);		
		
	}

	function contact()
	{
		$data['title'] = "Contact | Joao Saldanha University";
		$data['contacts'] = $this->frontend_model->get_contact()->row_array();
		$this->load->view('page_contact', $data);		
		
	}

	function send_mail_proccess()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			set_time_limit(0); 
			$this->load->library('email');
			$firstname 	= $this->input->post('fn');
			$lastName  	= $this->input->post('ln');
			$from 		= $this->input->post('email');
			$msg 		= $this->input->post('msg');
			$mails 		= 'admin@jsaldanha.org';
			$sbj 		= 'message from web jsaldanha.education';

			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html'; // Append This Line
			$this->email->initialize($config);

			$this->email->from($from, $firstname.' '.$lastName);
			$this->email->subject($sbj);
			$this->email->message($msg);	
			$this->email->to($mails);
			$this->email->send();
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-info','Message successful send'));
			redirect(base_url().'contact_us');
		}else{
			redirect(base_url().'contact_us');
		}
		
	}

	function library()
	{
		$data['title'] = "Library | Joao Saldanha University";
		$data['listFaculty'] = $this->frontend_model->get_list_lib_faculty();
		$this->load->view('page_library', $data);	
	}

	function get_detail_library($id)
	{
		$data['title'] = "Library | Joao Saldanha University";
		$data['listDeptList'] = $this->frontend_model->get_list_lib_dept($id);
		$this->load->view('page_detail_library', $data);	
	}

	function get_detail_library_list($id)
	{
		$data['title'] = "Library | Joao Saldanha University";
		$data['listLibs'] = $this->frontend_model->get_list_lib_book($id);
		$this->load->view('page_detail_list_library', $data);	
	}

	function students()
	{
		if ($this->session->userdata('oauth_valid')) {
			redirect(base_url().'students/dashboard');
		}else{
			$data['title'] = "Students | Joao Saldanha University";
			$this->load->view('page_student', $data);	

		}
	}

	function student_dashboard()
	{
		if ($this->session->userdata('oauth_valid')) {
			$student_id = $this->session->userdata('ses_student_id');
			$dept_id 	= $this->session->userdata('ses_dept_id');
			$data['compl_unit'] = $this->frontend_model->get_unit_comp($dept_id)->row_array();
			$data['total_unit'] = $this->frontend_model->get_full_unit($dept_id)->row_array();
			$data['stu_detail'] = $this->frontend_model->get_detail_student($student_id)->row_array();
			$data['score_file'] = $this->frontend_model->get_list_score_file($student_id);
			$data['title'] = "Students Dashboard | Joao Saldanha University";
			$this->load->view('page_student_dashboard', $data);
		}else{
			redirect(base_url().'students');
		}
	}


	function onlinereg()
	{
		$data['title'] = "Online Registration | Joao Saldanha University";
		$data['lisPro'] = $this->frontend_model->get_list_program();
		$this->load->view('page_onlinereg', $data);
	}

	function get_list_program_faculty($program)
	{
		if ($this->input->is_ajax_request()) {
			$getList = $this->frontend_model->get_list_faculty_by_program($program);
			if ($getList->num_rows() > 0) {
				echo "<option value='' disabled selected>Select School or Faculty</option>";
				foreach ($getList->result() as $lists) {
					echo "<option value='".$lists->id."'>".$lists->faculty_name."</option>";
				}
			}else{
				echo 0;
			}
		}else{
			redirect(base_url());
		}
	}

	function get_list_department($id)
	{
		if ($this->input->is_ajax_request()) {
			$getList = $this->frontend_model->get_list_department($id);
			if ($getList->num_rows() > 0) {
				echo "<option value='' disabled selected>Select Department</option>";
				foreach ($getList->result() as $lists) {
					echo "<option value='".$lists->id."'>".$lists->department_name."</option>";
				}
			}else{
				echo 0;
			}
		}else{
			redirect(base_url());
		}

	}

	function submit_online_registration_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$program = $this->input->post('program');
			$school = $this->input->post('school');
			$department = $this->input->post('department');
			$magister_depart = $this->input->post('magister_depart');
			$full_name = $this->input->post('full_name');
			$gender = $this->input->post('gender');
			$place_birth = $this->input->post('place_birth');
			$birthday = $this->input->post('birthday');
			$mobile_phone = $this->input->post('mobile_phone');
			$phone_number = $this->input->post('phone_number');
			$email = $this->input->post('email');
			$address = $this->input->post('address');
			$depId = 0;
			// upload to folder photo
			$newName = strtolower(seo_url($full_name).'-'.$_FILES['photoFile']['name']);
			$uploaded_file = $this->uploaded_files('photoFile','./_media/_students/',$newName);

			// check if magister or undergraduate dept
			if ($program=="magister") {
				$depId = $magister_depart;
			}else{
				$depId = $department;
			}

			$data = array(
					'student_id' 			=> 'new_student_'.$this->random_string(),
					'class_id'				=> 0,
					'department_id'			=> $depId,
					'student_name'			=> $full_name,
					'student_birth_place'	=> $place_birth,
					'student_birthday'		=> $birthday,
					'student_photo'			=> $uploaded_file['file_name'],
					'student_phone'			=> $phone_number,
					'student_mobile'		=> $mobile_phone,
					'student_addr'			=> $address,
					'student_status'		=> 'New Student',
					'gender'				=> $gender,
					'student_email'			=> $email
				);
			$ins = $this->frontend_model->insert_new_student($data);
			if ($ins) {
				redirect(base_url().'onlinereg_scs');
			}

		}else{
			redirect(base_url().'onlinereg');
		}
	}

	function onlinereg_scs()
	{
		$data['title'] = "Online Registration | Joao Saldanha University";
		$this->load->view('page_onlinereg_scs', $data);
	}

	function uploaded_files($filename,$upload_path,$newName){
		$this->load->library('upload');
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = '*';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $newName;
		// $config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->upload->initialize($config);

		if (!$this->upload->do_upload($filename))
		{
		    // case - failure
		    $upload_error = array('error' => $this->upload->display_errors());
		}
		else
		{
		    // case - success
		    $upload_data = $this->upload->data();
		    return $upload_data;
		}
    }

    function random_string() {
	    $key = '';
	    $keys = range(0, 9);

	    for ($i = 0; $i < 10; $i++) {
	        $key .= $keys[array_rand($keys)];
	    }

	    return $key;
	}

	function news_and_event()
	{
		$data['title'] = "News & Events | Joao Saldanha University";
		$data['listEvent'] = $this->frontend_model->get_list_news_event();
		$this->load->view('page_news_event', $data);	
	}

	function news_and_event_detail()
	{
		$url_en = $this->uri->segment(3);
		$data['detail'] = $this->frontend_model->get_detail_news_event($url_en)->row_array();
		$data['title'] = humanize($url_en, '-')." | Joao Saldanha University";
		$this->load->view('page_news_event_detail', $data);	

	}

	function admissions()
	{
		$data['title'] = "Admissions | Joao Saldanha University";
		$data['listEvent'] = $this->frontend_model->get_list_admissions();
		$this->load->view('page_admissions', $data);	
	}

	function admissions_detail()
	{
		$url_en = $this->uri->segment(3);
		$data['detail'] = $this->frontend_model->get_detail_admissions($url_en)->row_array();
		$data['title'] = humanize($url_en, '-')." | Joao Saldanha University";
		$this->load->view('page_admissions_detail', $data);	

	}

}

/* End of file Frontend.php */
/* Location: ./application/modules/frontend/controller/Frontend.php */