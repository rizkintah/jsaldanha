<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_model extends CI_Model {

	function get_slides() {
		return $this->db->get('_slides');
	}
	
	function get_facilities()
	{
		$this->db->where('parent_id', 0);
		$query = $this->db->get('_facilities');

		return $query;
	}

	function get_detail_facility($id)
	{
		$this->db->where('parent_id', $id);
		return $this->db->get('_facilities');
	}

	function get_about()
	{
		$this->db->where('content_name', 'about_jsu');
		$query = $this->db->get('_contents');

		return $query;
	}

	function get_contact()
	{
		$this->db->where('content_name', 'contact');
		$query = $this->db->get('_contents');

		return $query;
	}

	function get_faculty_av()
	{
		$query = $this->db->query("SELECT program_name, program_url, program_desc, faculty
								   FROM {TBL_PRE}_v_programs");
		return $query->result();
	}

	function get_faculty_planning()
	{
		$query = $this->db->query("SELECT *
								   FROM {TBL_PRE}_faculty
								   WHERE faculty_stat='Planning'");
		return $query;
	}	

	function get_detail_faculty($slug)
	{
		$this->db->where('slug', $slug);
		$query = $this->db->get('_faculty');
		return $query;
	}

	function get_list_faculty_department($facultyID)
	{
		$this->db->where('faculty_id', $facultyID);
		$query = $this->db->get('_department');
		return $query->result();
	}

	function get_staff()
	{
		$this->db->where('content_name', 'staffs');
		$query = $this->db->get('_contents');
		return $query;
	}

	function get_galleries()
	{
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('_galleries');
		return $query;
	}

	function get_list_lib_faculty()
	{
		return $this->db->get('_faculty');
	}

	function get_list_lib_dept($id)
	{
		$this->db->where('faculty_id', $id);
		return $this->db->get('_department');
	}

	function get_list_lib_book($id)
	{
		$this->db->where('department_id', $id);
		return $this->db->get('_library');
	}


	// student
	function get_unit_comp($dept_id)
	{
		$query = $this->db->query("	SELECT SUM(B.courses_sks) UNIT_COMPLETE
									FROM {TBL_PRE}_score_file A, {TBL_PRE}_courses B
									WHERE A.grade_id = B.grade_id
									AND B.department_id='$dept_id'");
		return $query;
	}

	function get_full_unit($dept_id)
	{
		$query = $this->db->query("	SELECT SUM(courses_sks) total
									FROM {TBL_PRE}_courses
									WHERE department_id='$dept_id'");
		return $query;
	}

	function get_detail_student($student_id)
	{
		$query = $this->db->query("	SELECT A.student_id, A.student_name, B.faculty_name, C.department_name
																FROM {TBL_PRE}_student A, {TBL_PRE}_faculty B, {TBL_PRE}_department C
																WHERE A.department_id = B.id
																AND B.id = C.faculty_id
																AND A.student_id='$student_id'");
		return $query;
	}

	function get_list_score_file($student_id)
	{
		$query = $this->db->query("	SELECT *
									FROM {TBL_PRE}_score_file
									WHERE student_id='$student_id'");
		return $query;
	}

	function get_list_program()
	{
		return $this->db->get('_programs');
	}

	function get_list_faculty_by_program($program)
	{
		$this->db->where('program_url', $program);
		$this->db->select('id');
		$idProg = $this->db->get('_programs')->row_array();

		$this->db->where('program_id', $idProg['id']);
		$this->db->where('faculty_stat', 'Active');
		$listFac = $this->db->get('_faculty');

		return $listFac;
	}

	function get_list_department($id)
	{
		$this->db->where('faculty_id', $id);
		return $this->db->get('_department');
	}

	function insert_new_student($data)
	{
		return $this->db->insert('_student',$data);
	}

	function get_list_news_event()
	{
		$query = $this->db->query(" SELECT id,url_en,title_en,date_en,fnStripTags(CONCAT(SUBSTRING(content_en,1, 300),' ...')) content_en
								  	FROM {TBL_PRE}_news_event");
		return $query;
	}

	function get_detail_news_event($url_en)
	{
		$this->db->where('url_en', $url_en);
		return $this->db->get('_news_event');
	}

	function get_list_admissions()
	{
		$query = $this->db->query(" SELECT id,url_en,title_en,date_en,fnStripTags(CONCAT(SUBSTRING(content_en,1, 300),' ...')) content_en
								  	FROM {TBL_PRE}_admissions");
		return $query;
	}

	function get_detail_admissions($url_en)
	{
		$this->db->where('url_en', $url_en);
		return $this->db->get('_admissions');
	}

}

/* End of file Frontend_model.php */
/* Location: ./application/modules/frontend/models/Frontend_model.php */