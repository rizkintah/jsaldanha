<?php include 'page_header.php'; ?>
	<p class="mt-15">
		JSU currently has four schools that offer Undergraduate degree (BSc) and Graduate Program that offer
		Master’s Degree (MSc) in development studies.
	</p>
	<p>
		The Schools are: 
	</p>


	<div class="panel-groups panel-group-schools" id="accordion" role="tablist" aria-multiselectable="true">
		<?php foreach ($schAv as $k => $v): ?>
			<div class="panel panel-white">
		    <div class="panel-heading" role="tab" id="<?=$k?>" title="Click for show detail" data-toggle="collapse" data-target="#<?=date('is')+$k?>">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse"  href="#<?=date('is')+$k?>" aria-expanded="true" aria-controls="<?=date('is')+$k?>">
		          <?=$v->program_name?>
		        </a>
		        <i data-feather="chevron-down" class="arrow-icon"></i>
		      </h4>
		    </div>

		    	
		    <div id="<?=date('is')+$k?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="<?=$k?>">
		      <div class="panel-body">
		      	<div class="program-desc">
		      		<?=$v->program_desc?>
		      	</div>

			    	<?php if (!empty($v->faculty)): ?>
			      	<div class="list-group list-group-faculty">
				        <?php  
				        	$arrFaculty = explode(",", $v->faculty);
									foreach($arrFaculty as $k => $v) {
										$arrDetailFaculty = explode("||", $v);
										?>
											<a href="<?=current_url().'/'.$arrDetailFaculty[0]?>" class="list-group-item"><?=$arrDetailFaculty[1]?></a>
										<?php
									}

				        ?>
			        </div>
			    	<?php endif ?>
		      </div>
		    </div>

		  </div>
		<?php endforeach ?>
	</div>
<?php include 'page_footer.php'; ?>