<?php include 'page_header.php'; ?>
<style type="text/css" media="screen">
.panel { position: relative; overflow: hidden; display: block; border-radius: 0 !important;  }
.panel-default { border-color: #b22222 !important; border:0px; }
.panel .panel-body { position: relative; padding: 0 !important; overflow: hidden; height: auto; }
.panel .panel-body a { overflow: hidden; }
.panel .panel-body a img { display: block; margin: 0; width: 100%; height: auto; border-top-left-radius:20px;  border-top-right-radius:20px; 
transition: all 0.5s; 
-moz-transition: all 0.5s; 
-webkit-transition: all 0.5s; 
-o-transition: all 0.5s; 
}
.panel .panel-body a.zoom:hover img { transform: scale(1.3); -ms-transform: scale(1.3); -webkit-transform: scale(1.3); -o-transform: scale(1.3); -moz-transform: scale(1.3); }
.panel .panel-body a.zoom span.overlay { position: absolute; top: 0; left: 0; visibility: hidden; height: 100%; width: 100%; opacity: 0; background: #444;
transition: opacity .25s ease-out;
-moz-transition: opacity .25s ease-out;
-webkit-transition: opacity .25s ease-out;
-o-transition: opacity .25s ease-out;
}     
.panel .panel-body a.zoom:hover span.overlay { display: block; visibility: visible; opacity: 0.75;; }  
.panel .panel-body a.zoom:hover span.overlay i { position: absolute; top: 45%; left: 0%; width: 100%; font-size: 2.25em; color: #fff !important; text-align: center;
opacity: 1;
-moz-opacity: 1;
-webkit-opacity: 1;
filter: alpha(opacity=1);    
-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=1)";
}
.panel .panel-body a.zoom:hover span.overlay .overlay-text {position: absolute; top: 35%; left: 2.5%; width: 95%; padding:8px; background: #000; color: #fff;
opacity:1;font-weight:bold; font-size:13px; border-radius:5px;}

.panel .panel-footer { padding: 8px !important; background-color: #f9f9f9 !important;  border:1px #ccc solid; }	
.panel .panel-footer h4 { display: inline; font: 400 normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #34495e margin: 0 !important; padding: 0 !important; }
.panel .panel-footer i.glyphicon { display: inline; font-size: 1.125em; cursor: pointer; }
.panel .panel-footer i.glyphicon-thumbs-up { color: #1abc9c; }
.panel .panel-footer i.glyphicon-thumbs-down { color: #e74c3c; padding-left: 5px; }
.panel .panel-footer div { width: 15px; display: inline; font: 300 normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #34495e; text-align: center; background-color: transparent !important; border: none !important; }	

.modal-title { font: 400 normal 1.625em "Roboto",Arial,Verdana,sans-serif; }
.modal-footer { font: 400 normal 1.125em "Roboto",Arial,Verdana,sans-serif; } 
</style>
	<p></p>
	<section class="row">
		<?php  
			foreach ($gal->result() as $imgGal) {
				?>
					<article class="col-xs-12 col-sm-6 col-md-3">
			            <div class="panel panel-success">
			                <div class="panel-body">
			                    <a href="<?=base_url()?>/_media/galleries/<?=$imgGal->img_filename?>" title="<?=$imgGal->title?>" class="zoom" data-title="<?=$imgGal->title?>" data-footer="<?=$imgGal->title?>" data-type="image" data-toggle="lightbox">
			                        <img src="<?= base_url() ?>timthumb.php?src=<?=base_url()?>/_media/galleries/<?=$imgGal->img_filename?>&h=350&w=350&z0=0" class="img-responsive" />
			                        <span class="overlay"><i class="glyphicon glyphicon-fullscreen"></i></span>
			                    </a>
			                </div>
			                <div class="panel-footer">
			                    <h4><?=character_limiter($imgGal->title, 30)?></h4>
			                </div>
			            </div>
			        </article>
				<?php
			}
		?>
	</section>
<?php include 'page_footer.php'; ?>
<script type="text/javascript">
	$(document).ready(function() {                 
	    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
	        event.preventDefault();
	        $(this).ekkoLightbox();
	    });                                        
	}); 
</script>