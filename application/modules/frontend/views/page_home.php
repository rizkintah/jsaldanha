<?php include 'page_header.php'; ?>
	<div class="welcome-text">
		<h2><span>WELCOME TO</span> Joao Saldanha University</h2>
		<h4>"Southeast's Global University"</h4>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="green">
				<article class="text-justify">
					<?php 
						if(isset($this->rzkt_lib->extract_img_text($about['content_txt'])[0])){
						?>
							<img src="<?=$this->rzkt_lib->extract_img_text($about['content_txt'])[0]?>" class="pull-left" style="width:125px;padding-right:10px;">
						<?php
						}
					?>
					<?=substr(strip_tags($about['content_txt']),0,630)?>
				</article>
				<a href="<?=base_url()?>about_jsu" class="link-more">More...</a>
			</div>
		</div>
		<div class="col-md-8">
			<div class="yellow">
				<div id="home_slide" class="carousel carFullscreen slide" data-ride="carousel">
				  <!-- Indicators -->			
				  <ol class="carousel-indicators">
				    <?php  
						$i=0;
						foreach ($listSlide->result() as $indcats) {
							?>
								<li data-target="#home_slide" data-slide-to="<?=$i?>" class="carFullscreen-indicator"></li>			
							<?php
							$i++;
						}
					?>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				  	<?php  
				  		foreach ($listSlide->result() as $imgs) {
				  			?>
			  				 	<div class="item">
							        <img src="<?=base_url()?>_media/galleries/<?=$imgs->img_file?>" alt="First Image">
							    </div>
				  			<?php
				  		}
				  	?>
				  </div>
				</div>
			</div>
		</div>
	</div>
<?php include 'page_footer.php'; ?>