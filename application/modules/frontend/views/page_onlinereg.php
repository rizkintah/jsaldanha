<?php include 'page_header.php'; ?>
	<h4 class="page-header">
		Online Admission
		<div>
			<small style="color:#F89939;">Register as a new JSU Student Online.</small>
		</div>
	</h4>

	<div class="list-group">
	  <a href="#" class="list-group-item active" style="background:#FFDD87;border-color:#FFDD87;color:#8A6D3B;">
	  	Online Admission Form Manual
	  </a>
	  <a href="#" class="list-group-item">
	  	<i class="glyphicon glyphicon-arrow-right" style="font-size:10px;color:#ffbc11;"></i>&nbsp; 
	  		<font style="color:#947B4E;">Fill all fields with your valid informations</font>
	  </a>
	  <a href="#" class="list-group-item">
	  	<i class="glyphicon glyphicon-arrow-right" style="font-size:10px;color:#ffbc11;"></i>&nbsp; 
	  		<font style="color:#947B4E;">Email and Handphone Number will be used for admission confirmation</font>
	  </a>
	  <a href="#" class="list-group-item">
	  	<i class="glyphicon glyphicon-arrow-right" style="font-size:10px;color:#ffbc11;"></i>&nbsp; 
	  		<font style="color:#947B4E;">Please recheck your admission form before you submit the form</font>
	  </a>
	</div>

	<!-- form -->
		<div class="alert alert-warning" role="alert">Admission Form</div>
		<div id="alertArea"></div>
		<form action="<?=base_url()?>submit_online_registration" method="POST" class="form-horizontal" id="frmReg" enctype="multipart/form-data">
			<div class="form-group">
				<label for="inputProgram" class="col-sm-2">Program</label>
				<div class="col-sm-10">
				  <input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  <select name="program" id="inputProgram" class="form-control" required="required">
				  	<option value="" disabled selected>Select Program</option>
				  	<?php  
				  		foreach ($lisPro->result() as $progs) {
				  			?>
				  				<option value="<?=$progs->program_url?>"> <?=$progs->program_name?> </option>
				  			<?php
				  		}
				  	?>
				  </select>
				</div>
			</div>
			<!-- area undergraduate -->
			<div id="areaundergraduate" style="display:none;">
				<div class="form-group">
					<label for="inputundergraduate" class="col-sm-2">School</label>
					<div class="col-sm-10">
					  <select name="school" id="inputundergraduate" class="form-control frmCheck frmCheck2" required="required">
					  	<option value=""></option>
					  </select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputdepartment" class="col-sm-2">Department</label>
					<div class="col-sm-10">
					  <select name="department" id="inputdepartment" class="form-control frmCheck" required="required">
					  	<option value="" disabled selected>Select Department</option>
					  </select>
					</div>
				</div>
			</div>
			<!-- end area undergraduate -->
			<!-- area magister -->
			<div class="form-group" id="areamagister" style="display:none;">
				<label for="inputmagister" class="col-sm-2">Graduate Program/MSc</label>
				<div class="col-sm-10">
				  <select name="magister_depart" id="inputmagister" class="form-control frmCheck" required="required">
				  	<option value=""></option>
				  </select>
				</div>
			</div>
			<!-- end area magister -->
			<div class="form-group">
				<label for="fullname" class="col-sm-2">Full Name</label>
				<div class="col-sm-10">
				  <input type="text" name="full_name" class="form-control frmCheck" id="fullname" placeholder="Full Name..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="photo" class="col-sm-2">Photo</label>
				<div class="col-sm-10">
					<div class="input-group">
				  	  <input type="file" name="photoFile" class="form-control frmCheck" id="photo" required>
					  <span class="input-group-addon" id="cn">Photo Size (3x4 cm)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="inputgender" class="col-sm-2">Gender</label>
				<div class="col-sm-10">
				  <select name="gender" id="inputgender" class="form-control frmCheck" required="required">
				  	<option value="" disabled selected>Select Gender</option>
				  	<option value="Men">Men</option>
				  	<option value="Women">Women</option>
				  </select>
				</div>
			</div>
			<div class="form-group">
				<label for="place_birth" class="col-sm-2">Place of birth</label>
				<div class="col-sm-10">
				  <input type="text" name="place_birth" class="form-control frmCheck" id="place_birth" placeholder="Place of birth..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="birthday" class="col-sm-2">Date of Birth</label>
				<div class="col-sm-10">
				  <input type="text" name="birthday" class="form-control frmCheck datePick" id="birthday" placeholder="Date of birth..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="cn" class="col-sm-2">Contact Number</label>
				<div class="col-sm-5">
					<div class="input-group">
					  <input type="text" name="mobile_phone" class="form-control frmCheck" placeholder="Mobile number..." aria-describedby="basic-addon2" required>
					  <span class="input-group-addon" id="cn">Mobile Number</span>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="input-group">
					  <input type="text" name="phone_number" class="form-control frmCheck" placeholder="Phone (Land Line)..." aria-describedby="basic-addon2" required>
					  <span class="input-group-addon" id="ph">Phone (Land Line)</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-2">Email</label>
				<div class="col-sm-10">
				  <input type="text" name="email" class="form-control frmCheck" id="email" placeholder="Email..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="address" class="col-sm-2">Address</label>
				<div class="col-sm-10">
					<textarea name="address" id="address" class="form-control frmCheck noresize" rows="4" required="required" placeholder="Address..."></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<p class="text-warning">
						Please <strong>recheck your registration form</strong> before
						click Submit This Admission Form to submit your data.
					</p>
				  	<input type="submit" class="btn btn-primary frmCheck" value="Submit This Admission Form">
				</div>
			</div>
		</form>
	<!-- end form -->
		
<?php include 'page_footer.php'; ?>