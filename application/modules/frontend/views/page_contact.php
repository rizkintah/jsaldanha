<?php include 'page_header.php'; ?>
	<p></p>
	<div class="row">
		<div class="col-md-6">
			<?=$contacts['content_txt']?>
		</div>
		<div class="col-md-5 col-md-offset-1">
			<div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title" style="font-size:16px;font-weight:bold;">Contact</h3>
              </div>
              <div class="panel-body">
              	<?=$this->session->flashdata('alertFlash')?>
                <form action="<?=base_url('send_mail')?>" method="POST" >
				  <div class="form-group">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				    <label for="fn">First Name</label>
				    <input type="text" name="fn" class="form-control" id="fn" placeholder="First Name..." required>
				  </div>
				  <div class="form-group">
				    <label for="ln">Last Name</label>
				    <input type="text" name="ln" class="form-control" id="ln" placeholder="Last Name..." required>
				  </div>
				  <div class="form-group">
				    <label for="email">Email</label>
				    <input type="email" name="email" class="form-control" id="email" placeholder="Email Address..." required>
				  </div>
				  <div class="form-group">
				    <label for="msg">Message</label>
				    <textarea name="msg" id="msg" class="form-control" rows="3" placeholder="Messages..." required></textarea>
				  </div>
				  <button type="submit" class="btn btn-primary pull-right">Send Message</button>
				</form>
              </div>
            </div>
		</div>
	</div>
<?php include 'page_footer.php'; ?>