<?php include 'page_header.php'; ?>
  <p></p>
    <div class="row">
      <div class="col-md-6">
        <table class="table">
          <tr>
            <td>Student Name</td>
            <td>:</td>
            <td><?=$stu_detail['student_name']?></td>
          </tr>
          <tr>
            <td>Student ID</td>
            <td>:</td>
            <td><?=$stu_detail['student_id']?></td>
          </tr>
          <tr>
            <td>Faculty</td>
            <td>:</td>
            <td><?=$stu_detail['faculty_name']?></td>
          </tr>
          <tr>
            <td>Department</td>
            <td>:</td>
            <td><?=$stu_detail['department_name']?></td>
          </tr>
        </table>

        <!-- middle area -->
        <h5><strong>Degrees progress</strong></h5>
            <table class="table" style="width:45%;border:none !important;">
               <tr style="margin-bottom:10px;">
                   <td style="border:none !important;padding:5px;"><strong>COMPLETED</strong></td>
                   <td style="background:#01B302;color: #ffffff;text-align:center;font-weight:bold;padding:5px;border:2px solid #fff;"><?php if(empty($compl_unit['UNIT_COMPLETE'])){echo 0;}else{echo $compl_unit['UNIT_COMPLETE'];}?> Units</td>
               </tr>
               <tr style="margin-top:20px;">
                   <td style="border:none !important;padding:5px;"><strong>TO DO</strong></td>
                   <td style="background:#01B302;color: #ffffff;text-align:center;font-weight:bold;padding:5px;border:2px solid #fff;"><?=$total_unit['total']?> Units</td>
               </tr>
            </table>
      </div>
      <div class="col-md-6">
            <div class="panel panel-default">
               <div class="panel-heading">
                   <h3 class="panel-title"><strong>TEST SCORE</strong></h3>
               </div>
               <div class="panel-body">
                   <div class="table-responsive">
                        <table class="table table-striped">
                            <?php  
                              if ($score_file->num_rows() > 0) {
                                foreach ($score_file->result() as $filesScore) {
                                    ?>
                                        <tr>
                                           <td>Grade <?=$filesScore->grade_id?></td>
                                           <td class="text-right">
                                               <a href="<?=base_url()?>_media/_var/<?=$filesScore->file_score?>" title="Download" class="btn btn-xs btn-primary" download>Download</a>
                                           </td>
                                        </tr>
                                    <?php
                                }
                              }else{
                                ?>
                                  <tr>
                                    <td colspan="2" align="center">No Data Available</td>
                                  </tr>
                                <?php
                              }
                            ?>
                        </table>
                    </div>
               </div>
           </div>   
        </div>
    </div>
<?php include 'page_footer.php'; ?>