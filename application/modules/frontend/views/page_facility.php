<?php include 'page_header.php'; ?>
	<p></p>
	<ul style="list-style:square;">
		
		<?php  
			foreach ($facilities->result() as $facs) {
				?>
					<li>
						<a href="<?=base_url()?>facility/detail/<?=$facs->id?>-<?=seo_url($facs->fac_name)?>" style="color:#434A54;">
							<h4><?=strtoupper($facs->fac_name)?></h4>
						</a>
					</li>
				<?php
			}
		?>
		<li>
			<a href="<?=base_url()?>library" title="Library" style="color:#434A54;">
				<h4>LIBRARY</h4>
			</a>
		</li>
	</ul>
<?php include 'page_footer.php'; ?>