<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$title?></title>
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link href="<?=base_url()?>_assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/bootflat.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/style.css?v=<?=date("is")?>" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/ekko-lightbox.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/student.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/plugins/nprogress/nprogress.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/plugins/bs-datepicker/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
    <link rel="shortcut icon" type="image/png" href="<?=base_url()?>_assets/img/favicon-32x32.png" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <input type="hidden" id="refreshed" value="no">
    <header id="header">
        <div class="logo">
            <div class="container">
                <img src="<?=base_url()?>_assets/img/header.png" alt="jsu">
            </div>
        </div>   

        <nav class="navbar navbar-default navbar-jsu" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".menu_responsive">
                        <span><i class="glyphicon glyphicon-align-justify"></i> Menu</span>
                    </button>
                    <a class="navbar-brand hidden-md hidden-lg" href="<?=base_url()?>">JSU</a>
                </div>
        
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse menu_responsive scrollable-menu">
                    <ul class="nav navbar-nav">
                        <li><a href="<?=base_url()?>about_jsu">About JSU</a></li>
                        <li><a href="<?=base_url()?>schools">Schools</a></li>
                        <li><a href="<?=base_url()?>admissions">Admissions</a></li>
                        <li><a href="<?=base_url()?>facility">Facilities</a></li>
                        <li><a href="<?=base_url()?>staffs">Staffs</a></li>
                        <li><a href="<?=base_url()?>students">Students</a></li>
                        <li><a href="<?=base_url()?>news_and_events">News &amp; Events</a></li>
                        <li><a href="<?=base_url()?>galleries">Galleries</a></li>
                        <li><a href="<?=base_url()?>contact_us">Contact Us</a></li>
                        <li><a href="<?=base_url()?>onlinereg" style="color:red;font-weight:bold;">Online Registration</a></li>
                    </ul>
                    <?php 
                        $sess = $this->session->userdata('oauth_valid');
                        if ($sess) {
                            ?>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                           <i class="glyphicon glyphicon-user"></i> <?=$this->session->userdata('sess_name')?> <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?=base_url()?>signout">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            <?php
                        }else{
                            ?>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="<?=base_url()?>students">SIGN IN</a></li>
                                    <li><a href="#">REGISTER</a></li>
                                </ul>
                            <?php
                        }
                    ?>
                    
                    <div class="divider"></div>
                    <ul class="nav navbar-nav hidden-lg  hidden-md nav-sosial">
                        <li>
                            <a href="#"><img src="<?=base_url()?>_assets/img/fb-60.png" alt="Facebook"> Facebook</a>
                        </li>
                        <li>
                            <a href="#"><img src="<?=base_url()?>_assets/img/twitter-60.png" alt="Twitter"> Twitter</a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>

    </header><!-- /header -->

    <div class="container">
        