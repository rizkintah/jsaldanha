	</div>


	<footer class="footer">
      <div class="container">
   		<h5 class="text-muted">"Southeast's Global University"</h5>
        <ul class="nav nav-pills nav-footer hidden-xs hidden-sm">
            <li>
                <a href="https://www.facebook.com/Joao-Saldanha-University-103934049976533/" target="_blank"><img src="<?=base_url()?>_assets/img/fb-60.png" alt="Facebook"></a>
            </li>
            <li>
                <a href="https://twitter.com/jsaldanhae" target="_blank"><img src="<?=base_url()?>_assets/img/twitter-60.png" alt="Twitter"></a>
            </li>
        </ul>
      </div>
    </footer>

	<script src="<?=base_url()?>_assets/js/jquery-1.12.3.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://unpkg.com/feather-icons"></script>
	<script src="<?=base_url()?>_assets/js/apps.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/reg.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/bs-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/modernizr-custom.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/ekko-lightbox_2.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/nprogress/nprogress.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/jquery_validation/jquery.validate.min.js" type="text/javascript" ></script>
	<script src="<?=base_url()?>_assets/plugins/jquery_validation/additional-methods.min.js" type="text/javascript" ></script>
	<script type="text/javascript">
		
		$(document).ready(function  () {
			 var $item = $('.carFullscreen .item');
  			var $indicat = $('.carFullscreen-indicator');
			$item.eq(0).addClass('active');
  			$indicat.eq(0).addClass('active');
		});
		var encUrl = '<?=base_url()?>';
		onload=function(){
		var e=document.getElementById("refreshed");
		if(e.value=="no")e.value="yes";
		else{e.value="no";location.reload();}
		}

		$(window).scroll(function() {
		  if ($(document).scrollTop() > 50) {
		    $('#header').addClass('to_fixed');
		  } else {
		    $('#header').removeClass('to_fixed');
		  }
		});

		$(".navbar-collapse").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
		
	</script>
</body>
</html>