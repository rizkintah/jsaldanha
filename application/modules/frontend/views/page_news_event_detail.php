<?php include 'page_header.php'; ?>
	<p></p>
	<h4 style="text-transform: capitalize;"><?=$detail['title_en']?></h4>
	<br>
	<span class="text-muted"><?=date('d M Y',strtotime($detail['date_en']))?></span><br><br>
	<article>
		<?=$detail['content_en']?>
	</article>
<?php include 'page_footer.php'; ?>