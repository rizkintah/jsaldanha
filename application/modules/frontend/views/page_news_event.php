<?php include 'page_header.php'; ?>
	<p></p>
	<div class="list-group listStyleGroup">
		<?php  
			foreach ($listEvent->result() as $lists) {
				?>
					<a href="<?=base_url()?>news_and_events/read/<?=$lists->url_en?>" class="list-group-item" title="Read More...">
				        <h4 class="list-group-item-heading"><?=$lists->title_en?></h4>
				        <p class="list-group-item-text">
				        	<span class="text-muted"><?=date('d M Y',strtotime($lists->date_en))?></span><br><br>
				        	<?=$lists->content_en?>
				        </p>
			      	</a>
				<?php
			}
		?>
    </div>
<?php include 'page_footer.php'; ?>