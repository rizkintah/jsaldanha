<?php include 'page_header.php'; ?>
	<p></p>

	<table class="table table-striped table-hover table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center" width="10%">No</th>
				<th class="text-center">Title</th>
				<th class="text-center">Description</th>
				<th width="15%" class="text-center">Download Book</th>
			</tr>
		</thead>
		<tbody>
			<?php  
				$no = 1;
				if ($listLibs->num_rows() > 0) {
					foreach ($listLibs->result() as $lis) {
						?>
							<tr>
								<td class="text-center"><?=$no?></td>
								<td class="text-center"><?=$lis->lib_title?></td>
								<td class="text-center"><?=$lis->lib_desc?></td>
								<td class="text-center"> 
									<a href="<?=base_url()?>_media/_lib/<?=$lis->lib_file?>" title="Download" class="btn btn-primary btn-xs" target="_blank" download>
										Download
									</a>
								</td>
							</tr>
						<?php
						$no++;
					}
				}else{
					?>
						<tr>
							<td colspan="4" class="text-center">No Data Available</td>
						</tr>
					<?php
				}
			?>
		</tbody>
	</table>
<?php include 'page_footer.php'; ?>