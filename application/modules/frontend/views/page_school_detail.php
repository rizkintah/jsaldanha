<?php include 'page_header.php'; ?>
	<ol class="breadcrumb m-15">
    <li><a href="<?=site_url('schools')?>"><?=ucwords($this->uri->segment(1))?></a></li>
    <li class="active"><span><?=$detail['faculty_name']?></span></li>
  </ol>

	<h4 class="page-title"><?=$detail['faculty_name']?></h4>
	<div style="margin-bottom:50px">
		<?=$detail['faculty_desc']?>
	</div>



	<div class="panel-group-schools" id="accordion" role="tablist" aria-multiselectable="true">
		<?php foreach ($listDepartment as $k => $v): ?>
			<div class="panel panel-white">
		    <div class="panel-heading" role="tab" id="<?=$k?>" title="Click for show detail" data-toggle="collapse" data-target="#<?=date('is')+$k?>">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?=date('is')+$k?>" aria-expanded="true" aria-controls="<?=date('is')+$k?>">
		          <?=$v->department_name?>
		        </a>
		        <i data-feather="chevron-down" class="arrow-icon"></i>
		      </h4>
		    </div>

		    <?php if (!empty($v->department_desc)): ?>
		    	
		    <div id="<?=date('is')+$k?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="<?=$k?>">
		      <div class="panel-body">
		      	<?=$v->department_desc?>
		      </div>
		    </div>
		    <?php endif ?>

		  </div>
		<?php endforeach ?>
	</div>
<?php include 'page_footer.php'; ?>