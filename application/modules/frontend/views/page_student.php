<?php include 'page_header.php'; ?>
	<p></p>

    <div class="container">
    	<div class="frm-wrap">
	    	<form method="POST" role="form" class="frm-login" id="frmLoginStudent">
	    		<div class='alert alert-danger alert-dismissible fade in' role='alert' id="alertShow" style="display:none;">
		              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
		              </button>
		              Username or password wrong...
		        </div>
	    		<div class="frm-conten">
	    			<div class="form-group">
	    				<input type="hidden" class="form-control" value="webadmin" id="url_id">
	    				<input type="hidden" class="form-control" value="<?=$this->security->get_csrf_hash()?>" id="code_id">
	    				<label for="un">Username</label>
	    				<input type="text" name="username" class="form-control" id="un" placeholder="Enter Username" required>
	    			</div>
	    			<div class="form-group">
	    				<label for="pw">Password</label>
	    				<input type="password" name="password" class="form-control" id="pw" placeholder="Enter Password" required>
	    			</div>
	    			<!-- <div class="form-group">
				      	<a href="" title="">Forgot password?</a>
				  	</div> -->
	    			<button type="submit" class="btn btn-dark-grey btn-block" id="btn">LOGIN</button>
	    		</div>
    		</form>
    	</div>
    </div>
<?php include 'page_footer.php'; ?>
<script src="<?=base_url()?>_assets/js/login.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	var direct = "<?=base_url()?>";
	 $("#frmLoginStudent").validate({
        rules: {
            username: {
                required: true,
            },
            password : {
            	required :true
            }
            
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    });
</script>