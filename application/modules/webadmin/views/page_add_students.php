<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/students/add_students_process" method="POST" class="form-horizontal" id="frm-add-student" enctype="multipart/form-data">
			<div class="form-group">
				<label for="sid" class="col-sm-2 control-label">Student ID</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<input type="text" name="student_id" class="form-control" id="sid" placeholder="Type Student ID..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="programId" class="col-sm-2 control-label">Faculty Program</label>
				<div class="col-sm-9">
					<select name="program_id" id="programId" class="form-control" required="required">
						<option value="" selected disabled>Select Program</option>
						<option value="1">Undergraduate</option>
						<option value="2">Magister / Msc</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="sfac" class="col-sm-2 control-label">Faculty</label>
				<div class="col-sm-9">
					<select name="faculty_id" id="sfac" class="form-control" style="width:100%;" required="required">
					</select>
				</div>
			</div>
			<div class="form-group" id="deptForm">
				<label for="sdept" class="col-sm-2 control-label">Department</label>
				<div class="col-sm-9">
					<select name="department_id" id="sdept" class="form-control" style="width:100%;">
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="sname" class="col-sm-2 control-label">Student Status</label>
				<div class="col-sm-9">
					<select name="student_status" id="inputStudent_status" class="form-control" required="required">
						<option value="">Select Student Status</option>
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
						<option value="New Student">New Student</option>
						<option value="Graduate">Graduate</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="say" class="col-sm-2 control-label">Academic Year</label>
				<div class="col-sm-9">
				  	<input type="text" name="academic_year" class="form-control" id="say" placeholder="Type academic year...(ex : 2015)" required>
				</div>
			</div>
			<div class="form-group">
				<label for="sname" class="col-sm-2 control-label">Student Name</label>
				<div class="col-sm-9">
				  	<input type="text" name="student_name" class="form-control" id="sname" placeholder="Type Student Name..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="gender" class="col-sm-2 control-label">Gender</label>
				<div class="col-sm-9">
					<select name="gender" id="gender" class="form-control" required="required">
						<option value="" selected disabled>Select Gender</option>
						<option value="1">Men</option>
						<option value="2">Women</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="spass" class="col-sm-2 control-label">Student Password</label>
				<div class="col-sm-9">
				  	<input type="text" name="password" class="form-control" id="spass" placeholder="Type Password..." required>
				</div>
			</div>
			<div class="form-group">
				<label  class="col-sm-2 control-label">Student Birthday</label>
				<div class="col-sm-3">
				  	<input type="text" name="student_birth_place" class="form-control" placeholder="Place of birthday..." required>
				</div>
				<div class="col-sm-3">
				  	<input type="text" name="student_birthday" class="form-control datePick" placeholder="Date of birthday..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="sclass" class="col-sm-2 control-label">Student Class</label>
				<div class="col-sm-9">
					<select name="class_id" id="sclass" class="form-control" style="width:100%;" required>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="sphoto" class="col-sm-2 control-label">Student Photo</label>
				<div class="col-sm-9">
				  	<input type="file" name="student_photo" id="sphoto" class="form-control" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Student Phone</label>
				<div class="col-sm-9">
				  	<input type="text" name="student_phone" class="form-control" placeholder="Type student phone...">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Student HP</label>
				<div class="col-sm-9">
				  	<input type="text" name="student_mobile" class="form-control" placeholder="Type student handpone...">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Student Email</label>
				<div class="col-sm-9">
				  	<input type="email" name="student_email" class="form-control" placeholder="Type student email..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="ckEditor" class="col-sm-2 control-label">Student Address</label>
				<div class="col-sm-9">
				  <textarea name="student_addr" class="form-control" rows="3" required placeholder="Type student address..."></textarea>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/students" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Save Data Student</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="javascript:void(0)" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="javascript:void(0)" data-tooltip="true" title="Delete Data">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    }

    //validate
    $("#frm-add-student").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       },
	       academic_year:{
	       	required :true,
	       	number:true
	       }
	    },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
	});

    // select2 department
	$("#sfac").select2(  {
        placeholder: "Select Faculty",
        allowClear: true,
        // minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_fac_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.faculty_name };
                    })
                };
            }
        },
    });

    // select2 department
	$("#sdept").select2(  {
        placeholder: "Select Department",
        allowClear: true,
        // minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_dept_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.department_name };
                    })
                };
            }
        },
    });

     // select2 class
	$("#sclass").select2(  {
        placeholder: "Select Student Class",
        allowClear: true,
        // minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_class_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.class_name+' - '+obj.class_desc };
                    })
                };
            }
        },
    });

    // program 
    $("#programId").change(function () {
    	var vals = $(this).val();
    	if (vals==2) {
    		$("#deptForm").hide();
    	}else{
    		$("#deptForm").show();
    	}
    });


    
</script>