<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/manage_pages/update_contact_process" method="POST" class="form-horizontal" id="frm-add-faculty">
			<div class="teks">
			<div class="form-group">
				<label for="ckEditor" class="col-sm-1 control-label">Contact</label>
				<div class="col-sm-11">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<textarea name="contact" id="ckEditor" class="form-control" rows="3" required placeholder="Type contact..."><?=$detail['content_txt']?></textarea>
				</div>
			</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-1 col-sm-11">
				  <button type="submit" class="btn btn-primary">Save Contact</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>