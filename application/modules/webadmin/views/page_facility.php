<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/facilities/update_facilities_process" method="POST" class="form-horizontal" id="frm-add-faculty">
			<div class="teks">
			<div class="form-group">
				<label for="ckEditor" class="col-sm-1 control-label">Facilities</label>
				<div class="col-sm-11">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<textarea name="description" id="ckEditor" class="form-control" rows="3" required placeholder="Type faculty description..."><?=$detail['content_txt']?></textarea>
				</div>
			</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-1 col-sm-11">
				  <button type="submit" class="btn btn-primary">Save Facilities</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="javascript:void(0)" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="javascript:void(0)" data-tooltip="true" title="Delete Data">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    }

    //
    $("#frm-add-faculty").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       }
	    }
	});


    
</script>