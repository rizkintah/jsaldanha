<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/teachers/add_teacher_process" method="POST" class="form-horizontal" id="frm-add-teacher">
			<div class="form-group">
				<label class="col-sm-2 control-label">Lecture ID</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<input type="text" name="teacher_id" class="form-control" placeholder="Type lecture ID..." required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Lecture Name</label>
				<div class="col-sm-9">
				  	<input type="text" name="teacher_name" class="form-control" placeholder="Type lecture name..." required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Lecture Phone</label>
				<div class="col-sm-9">
				  	<input type="text" name="teacher_phone" class="form-control" placeholder="Type lecture phone...">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Lecture HP</label>
				<div class="col-sm-9">
				  	<input type="text" name="teacher_mobile" class="form-control" placeholder="Type lecture handpone...">
				</div>
			</div>
			<div class="form-group">
				<label for="ckEditor" class="col-sm-2 control-label">Lecture Address</label>
				<div class="col-sm-9">
				  <textarea name="teacher_addr" class="form-control" rows="3" required placeholder="Type address..."></textarea>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/lectures" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Add Lecture</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
    $("#frm-add-teacher").validate({
	    rules: {
			teacher_id : {
				required:true
			},
			teacher_name : {
				required:true
			},
			teacher_phone : {
				number : true
			},
			teacher_mobile : {
				number:true
			}
	    }
	});   
</script>