<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title">Management Scores</h3>
      </div>

      <div class="panel-body">
      		<?=$this->session->flashdata('alertFlash')?>
        	<table 	data-toggle="table"
        		data-toolbar="#toolbar"
	           	--data-search="true"
	           	data-show-refresh="true"
	           	data-show-toggle="true"
	           	data-show-columns="true"
	           	data-show-export="true"
	           	data-filter-control="true"
           		--data-filter-show-clear="true"
	           	--data-show-pagination-switch="true"
	           	data-pagination="true"
	           	data-page-size="20"
			   	data-side-pagination="server" 
			   	data-url="<?=base_url()?>webadmin/students/get_list_student_ajax"
	           	data-page-list="[20, 35, 50, 100]"
	           	class="table-striped table-hover">
			    <thead>
			        <tr>
			        	<th data-field="no" class="col-md-1" >no</th>
		                <th data-field="student_id" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_id</th>
		                <th data-field="department_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">department_name</th>
		                <th data-field="faculty_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">faculty_name</th>
		                <th data-field="class_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">class_name</th>
		                <th data-field="operate" data-events="operateEvents"  data-formatter="operateFormatter" data-align="center" data-cell-style="cellStyle">Action</th>
			        </tr>
			    </thead>
			</table>
      </div>
    </div>
<?php include 'page_footer.php'; ?>



<script type="text/javascript">

	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
	        '<a class="info btn btn-primary btn-xs" data-unique-id="'+row.id+'" data-toggle="uploadScoreModals" data-target="#uploadScoreModals'+row.id+'" data-backdrop="static">',
	        'Choose',
	        '</a>'
	    ].join('');
    };

     function cellStyle(value, row, index, field) {
      return {
        classes: 'text-nowrap another-class'
      };
    }

    window.operateEvents = {
	    'click .info': function (e, value, row) {
	        $('#uploadScoreModals').modal({backdrop: 'static'});
	        $("#student_id").val(row.student_id);
	        $("#student_name").val(row.student_name);
	        $("#inputGrade").val('');
	        $("#inputFile").val('');
	        $.ajax({
				type: 'GET',
				url: direct+"webadmin/scores/get_list_score",
				data: {'student_id': row.student_id},
				beforeSend: function () {
					$("#AreaHis").html("<tr><td colspan='4' align='center'>Loading...</td></tr>");
				},
				success: function (data) {
					$("#AreaHis").html(data);
				},
			   error: function (xhr, ajaxOptions, thrownError) {
		          console.log(xhr.status);
		          console.log(thrownError);
		          console.log(xhr.responseText);
		        }
			});
	        	// .find('.modal-body').html('<pre>' + 
	         //    JSON.stringify(row, null, 4) + '</pre>');
	    }
	};

</script>

<div class="modal fade" id="uploadScoreModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload & History Student Score</h4>
                <hr>
            </div>
            <div class="modal-body">
                <div class="row">
                	<div class="col-md-5">
                		<div class="panel panel-default">
			              <div class="panel-heading">
			                <h3 class="panel-title">Upload Score</h3>
			              </div>
			              <div class="panel-body">
			              	<form action="<?=base_url('webadmin/scores/upload_scores')?>" method="POST" role="form" id="frmScore" enctype="multipart/form-data">
			                	<div class="form-group">
			                		<label for="">Student ID</label>
	    							<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" id="token">
			                		<input type="text" name="student_id" class="form-control" id="student_id" readonly>
			                	</div>
			                	<div class="form-group">
			                		<label for="">Student Name</label>
			                		<input type="text" name="student_name" class="form-control" id="student_name" readonly>
			                	</div>
			                	<div class="form-group">
			                		<label for="">Grade</label>
			                		<select name="grade" id="inputGrade" class="form-control" required="required">
			                			<option value="">Select Grade</option>
			                			<option value="1">Grade 1</option>
			                			<option value="2">Grade 2</option>
			                			<option value="3">Grade 3</option>
			                			<option value="4">Grade 4</option>
			                			<option value="5">Grade 5</option>
			                			<option value="6">Grade 6</option>
			                			<option value="7">Grade 7</option>
			                			<option value="8">Grade 8</option>
			                		</select>
			                	</div>
			                	<div class="form-group">
			                		<label for="">Score File</label>
									<input type="file" name="score_file" id="inputFile" class="form-control" required="required">			                		
			                	</div>
			                	<button type="submit" class="btn btn-primary btn-block">Upload Score</button>
			                </form>
			              </div>
			            </div>
                	</div>
                	<div class="col-md-7">
                		<div class="panel panel-default">
			              <div class="panel-heading">History Score</div>
			              	<div class="table-responsive">
			              		<table class="table">
					                <thead>
					                  <tr>
					                    <th>#</th>
					                    <th>Grade</th>
					                    <th>File(s)</th>
					                    <th>Action</th>
					                  </tr>
					                </thead>
					                <tbody id="AreaHis"></tbody>
				              	</table>
				            </div>
			            </div>
                	</div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->