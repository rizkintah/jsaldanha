<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
      	<?=$this->session->flashdata('alertFlash')?>
		<div id="toolbar">
			<a href="<?=base_url()?>webadmin/manage_pages/add_admissions" title="Add Faculty" class="btn btn-default btn-sm">
				<i class="glyphicon glyphicon-plus"></i> Add Admission
			</a>
		</div>
    	<table 	data-toggle="table"
    		    data-toolbar="#toolbar"
           	--data-search="true"
           	data-show-refresh="true"
           	data-show-toggle="true"
           	data-show-columns="true"
           	data-show-export="true"
           	data-filter-control="true"
       		 --data-filter-show-clear="true"
           	--data-show-pagination-switch="true"
           	data-pagination="true"
           	data-page-size="20"
		   	    data-side-pagination="server" 
		   	    data-url="<?=base_url()?>webadmin/manage_pages/get_list_admissions_ajax"
           	data-page-list="[20, 35, 50, 100]"
           	class="table-striped table-hover">
		    <thead>
		        <tr>
		        	<th data-field="rownum" class="col-md-1" data-width="2%">No</th>
	                <th data-field="title_en" data-sortable="true" data-filter-control="input" data-width="30%">Title</th>
	                <th data-field="date_en" data-sortable="true" data-filter-control="input" data-width="10%">Date</th>
	                <th data-field="content_en" data-sortable="true" data-filter-control="input">Description</th>
	                <th data-field="operate" data-formatter="operateFormatter" data-align="center" class="col-md-1">Action</th>
		        </tr>
		    </thead>
		</table>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="edit_admissions/'+row.id+'/'+row.url_en+'" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="delete_admissions/'+row.id+'/'+row.url_en+'" data-tooltip="true" title="Delete Data" onclick="return confirm(\'Are you sure want to delete?\')">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
        console.log(value, row, index);
    };

    // window.operateEvents = {
    //     'click .edit_btn': function (e, value, row, index) {
    //         alert('You click like icon, row: ' + JSON.stringify(row));
    //         console.log(value, row, index);
    //     }
    // };
	
</script>