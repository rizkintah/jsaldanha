<!DOCTYPE html>
<html lang="en" moznomarginboxes mozdisallowselectionprint>
<head>
    <title><?=$title?></title>
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link href="<?=base_url()?>_assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/bootflat.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/style.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/plugins/nprogress/nprogress.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>_assets/plugins/select2/css/select2.css">
    <link href="<?=base_url()?>_assets/plugins/bootstrap-table/src/bootstrap-table.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/plugins/bs-datepicker/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
    <link rel="shortcut icon" type="image/png" href="<?=base_url()?>_assets/img/favicon-32x32.png" />
    <link href="<?=base_url()?>_assets/css/ekko-lightbox.min.css" rel="stylesheet" media="screen">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header id="header">
        <div class="logo">
            <div class="container">
                <img src="<?=base_url()?>_assets/img/header.png" alt="jsu">
            </div>
        </div>   

        <nav class="navbar navbar-default navbar-jsu" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".menu_responsive">
                        <span><i class="glyphicon glyphicon-align-justify"></i> Menu</span>
                    </button>
                    <a class="navbar-brand hidden-md hidden-lg" href="<?=base_url()?>">JSU</a>
                </div>
        
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse menu_responsive">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                               Manage Pages <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url()?>webadmin/manage_pages/manage_slide">Manage Slide</a></li>
                                <li><a href="<?=base_url()?>webadmin/manage_pages/about_jsu">About JSU</a></li>
                                <!-- <li><a href="<?=base_url()?>webadmin/facilities">Facilities</a></li> -->
                                <li><a href="<?=base_url()?>webadmin/manage_pages/staffs">Staffs</a></li>
                                <!-- <li><a href="#">Schools</a></li> -->
                                <li><a href="<?=base_url()?>webadmin/manage_pages/news_event">News &amp; Events</a></li>
                                <li><a href="<?=base_url()?>webadmin/manage_pages/admissions">Admissions</a></li>
                                <li><a href="<?=base_url()?>webadmin/manage_pages/galleries">Galleries</a></li>
                                <li><a href="<?=base_url()?>webadmin/manage_pages/contact">Contact Us</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Schools <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url()?>webadmin/programs">Manage Programs</a></li>
                                <li><a href="<?=base_url()?>webadmin/faculty">Manage Faculty</a></li>
                                <li><a href="<?=base_url()?>webadmin/department">Manage Department</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=base_url('webadmin/classroom')?>">Class</a></li>
                        <li><a href="<?=base_url('webadmin/lectures')?>">Lectures</a></li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Students <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url('webadmin/students')?>">Manage Students</a></li>
                                <li><a href="<?=base_url('webadmin/students/online_registration')?>">Manage Online Registration</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Courses <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url()?>webadmin/courses/manage_courses">Manage Courses</a></li>
                                <li><a href="<?=base_url()?>webadmin/courses/teacher_courses">Manage Teacher Courses & Class</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Facilities <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url()?>webadmin/facilities/manage_main_facilities">Main Category Facility</a></li>
                                <li><a href="<?=base_url()?>webadmin/facilities/manage_sub_facilities">Sub Category Facility</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=base_url('webadmin/library')?>">Library</a></li>
                        <li><a href="<?=base_url('webadmin/scores')?>">Upload Score</a></li>
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="glyphicon glyphicon-user"></i> Hi, <?=$this->session->userdata('nama_sess')?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- <li><a href="#">Setting</a></li> -->
                                <li><a href="<?=base_url()?>login/logout_proccess">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="divider"></div>
                    <ul class="nav navbar-nav hidden-lg  hidden-md nav-sosial">
                        <li>
                            <a href="#"><img src="<?=base_url()?>_assets/img/fb-60.png" alt="Facebook"> Facebook</a>
                        </li>
                        <li>
                            <a href="#"><img src="<?=base_url()?>_assets/img/twitter-60.png" alt="Twitter"> Twitter</a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>

    </header><!-- /header -->

    <div class="container">        