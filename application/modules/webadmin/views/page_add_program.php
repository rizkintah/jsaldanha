<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/programs/add_program_process" method="POST" class="form-horizontal" id="frm-add-faculty">
					<div class="form-group">
						<label for="fn" class="col-sm-2 control-label">Program Name</label>
						<div class="col-sm-9">
			    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
						  	<input type="text" name="program_name" class="form-control" id="fn" placeholder="Type program name..." required>
						</div>
					</div>

					<div class="form-group">
						<label for="fn" class="col-sm-2 control-label">Sort</label>
						<div class="col-sm-9">
						  	<input type="number" name="sort" class="form-control" min="1" max="100" value="<?=$Jmlprogram+1?>">
						</div>
					</div>
					
					<div class="form-group">
						<label for="ckEditor" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-9">
						  <textarea name="description" id="ckEditor" class="form-control" rows="3" required placeholder="Type faculty description..."></textarea>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-9">
						  <a href="<?=base_url()?>webadmin/programs" class="btn btn-danger">Cancel</a>
						  <button type="submit" class="btn btn-primary">Add Program</button>
						</div>
					</div>
				</form>
      </div>
    </div>
  </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
    $("#frm-add-faculty").validate({
    	ignore: [],  
	    rules: {
	      program_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       }
	    }
	});


    
</script>