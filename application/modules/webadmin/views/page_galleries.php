<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      	<div class="panel-body">
	      	<?=$this->session->flashdata('alertFlash')?>
			<div id="toolbar">
				<a href="<?=base_url()?>webadmin/manage_pages/add_images" title="Add Image" class="btn btn-default btn-sm">
					<i class="glyphicon glyphicon-plus"></i> Add Image
				</a>
			</div>
	    	<table 	data-toggle="table"
	    		    data-toolbar="#toolbar"
		           	--data-search="true"
		           	data-show-refresh="true"
		           	data-show-toggle="true"
		           	data-show-columns="true"
		           	data-show-export="true"
		           	data-filter-control="true"
		       		--data-filter-show-clear="true"
		           	--data-show-pagination-switch="true"
		           	data-pagination="true"
		           	data-page-size="20"
			   	    data-side-pagination="server" 
			   	    data-url="<?=base_url()?>webadmin/manage_pages/get_list_galleries_ajax"
		           	data-page-list="[20, 35, 50, 100]"
		           	class="table-striped table-hover">
			    <thead>
			        <tr>
			        	<th data-field="rownum" class="col-md-1" data-width="2%">No</th>
			        	<th data-field="title" data-filter-control="input" data-sortable="true" data-width="50%">Title</th>
		                <th data-field="img_filename" data-formatter="imgFormatter" data-sortable="true" data-filter-control="input" data-width="30%">Image(s)</th>
		                <th data-field="operate" data-formatter="operateFormatter" data-align="center" class="col-md-1">Action</th>
			        </tr>
			    </thead>
			</table>
      	</div>
    </div>
<?php include 'page_footer.php'; ?>
<script type="text/javascript">
	var url = '<?=base_url()?>';
	function imgFormatter(value, row, index) {
		return "<img src='"+url+"_media/galleries/"+row.img_filename+"' width='50%' />";
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-danger remove" href="<?=base_url()?>webadmin/manage_pages/delete_galleries/'+row.id+'" data-tooltip="true" title="Delete Data" onclick="return confirm(\'Are you sure want to delete this data?\')">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    };
</script>