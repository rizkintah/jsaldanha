<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title">Management Teacher Courses</h3>
      </div>

      <div class="panel-body">
      		<?=$this->session->flashdata('alertFlash')?>
			<div id="toolbar">
				<a href="<?=base_url()?>webadmin/courses/add_teacher_courses" title="Add Teacher Course" class="btn btn-default btn-sm">
					<i class="glyphicon glyphicon-plus"></i> Add Teacher Course
				</a>
			</div>
        	<table 	data-toggle="table"
        		data-toolbar="#toolbar"
	           	--data-search="true"
	           	data-show-refresh="true"
	           	data-show-toggle="true"
	           	data-show-columns="true"
	           	data-show-export="true"
	           	data-filter-control="true"
           		--data-filter-show-clear="true"
	           	--data-show-pagination-switch="true"
	           	data-pagination="true"
	           	data-page-size="20"
			   	data-side-pagination="server" 
			   	data-url="<?=base_url()?>webadmin/courses/get_list_teacher_courses_ajax"
	           	data-page-list="[20, 35, 50, 100]"
	           	class="table-striped table-hover">
			    <thead>
			        <tr>
			        	<th data-field="no" class="col-md-1" >No</th>
		                <th data-field="teacher_id" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">teacher_id</th>
		                <th data-field="teacher_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">teacher_name</th>
		                <th data-field="courses_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">course_name</th>
		                <th data-field="class_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">class_name</th>
		                <th data-field="class_desc" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">class_desc</th>
		                <th data-field="department_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">department_name</th>
		                <th data-field="operate" data-formatter="operateFormatter" data-align="center" class="col-md-1" data-cell-style="cellStyle">Action</th>
			        </tr>
			    </thead>
			</table>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="edit_teacher_course/'+row.id+'/'+row.teacher_id+'" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="delete_teacher_course/'+row.id+'/'+row.teacher_id+'" data-tooltip="true" title="Delete Data" onclick="return confirm(\'are you sure want to delete this data?\')">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    };

     function cellStyle(value, row, index, field) {
      return {
        classes: 'text-nowrap another-class'
      };
    }
	
</script>