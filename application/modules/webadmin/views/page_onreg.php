<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title">Management Online Registration</h3>
      </div>

      <div class="panel-body">
      		<?=$this->session->flashdata('alertFlash')?>
			<div id="toolbar">
				<a href="<?=base_url()?>webadmin/students/add_students" title="Add Students" class="btn btn-default btn-sm">
					<i class="glyphicon glyphicon-plus"></i> Add Data Students
				</a>
			</div>
        	<table 	data-toggle="table"
        		data-toolbar="#toolbar"
	           	--data-search="true"
	           	data-show-refresh="true"
	           	data-show-toggle="true"
	           	data-show-columns="true"
	           	data-show-export="true"
	           	data-filter-control="true"
           		--data-filter-show-clear="true"
	           	--data-show-pagination-switch="true"
	           	data-pagination="true"
	           	data-page-size="20"
			   	data-side-pagination="server" 
			   	data-url="<?=base_url()?>webadmin/students/get_list_student_onreg_ajax"
	           	data-page-list="[20, 35, 50, 100]"
	           	class="table-striped table-hover">
			    <thead>
			        <tr>
			        	<th data-field="no" class="col-md-1" >no</th>
		                <th data-field="student_id" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_id</th>
		                <th data-field="student_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_name</th>
		                <th data-field="student_birth_place" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_birth_place</th>
		                <th data-field="student_birthday" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_birthday</th>
		                <th data-field="department_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">department_name</th>
		                <th data-field="faculty_name" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">faculty_name</th>
		                <th data-field="student_email" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_email</th>
		                <th data-field="student_phone" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_phone</th>
		                <th data-field="student_mobile" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_mobile</th>
		                <th data-field="student_addr" data-sortable="true" data-filter-control="input" data-cell-style="cellStyle">student_address</th>
		                <th data-field="operate" data-formatter="operateFormatter" data-align="center" data-cell-style="cellStyle">Action</th>
			        </tr>
			    </thead>
			</table>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="view_detail_onreg/'+row.student_id+'" data-tooltip="true" title="View Detail" target="_blank">',
            '<i class="glyphicon glyphicon-eye-open"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="delete_student/'+row.student_id+'" data-tooltip="true" title="Delete Data" onclick="return confirm(\'are sure want to delete this data?\')">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    };

    function cellStyle(value, row, index, field) {
      return {
        classes: 'text-nowrap another-class'
      };
    }
	
</script>