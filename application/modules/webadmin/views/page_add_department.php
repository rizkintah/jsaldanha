<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/department/add_department_process" method="POST" class="form-horizontal" id="frm-add-department">
			<div class="form-group">
				<label for="fn" class="col-sm-2 control-label">Faculty Name</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<select class="form-control" name="faculty_id" required>
				  		<option value="" selected disabled>Select Faculty Name</option>
						<?php foreach($list_faculty->result() as $faculty):?>
							<option value="<?=$faculty->id?>"><?=$faculty->faculty_name?></option>
						<?php endforeach;?>
				  	</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="fadd" class="col-sm-2 control-label">Department Name</label>
				<div class="col-sm-9">
				  <input type="text" name="department_name" id="fadd" class="form-control" placeholder="Department name..." required>
				</div>
			</div>
			<div class="teks">
				<div class="form-group">
					<label for="ckEditor" class="col-sm-2 control-label">Description</label>
					<div class="col-sm-9">
					  <textarea name="description" id="ckEditor" class="form-control" rows="3" required placeholder="Type Department description..."></textarea>
					</div>
				</div>
			</div>
			
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/department" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Add Department</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="javascript:void(0)" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="javascript:void(0)" data-tooltip="true" title="Delete Data">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    }

    //
    $("#frm-add-department").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       }
	    }
	});


    
</script>