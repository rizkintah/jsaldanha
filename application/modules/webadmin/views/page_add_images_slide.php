<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/manage_pages/add_images_slide_process" method="POST" class="form-horizontal" id="frm-add-faculty" enctype="multipart/form-data">
			<div class="form-group">
    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				<label for="de" class="col-sm-2 control-label">Images</label>
				<div class="col-sm-9">
				  	<input type="file" name="img_filename" class="form-control" required>
				  	<label>(Image resolution 780px x 390px)</label>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/manage_pages/manage_slide" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Save Image</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
    $("#frm-add-faculty").validate({
    	ignore: [],  
	    rules: {
	      title: {
	      	required: true
	      },
	      img_filename: {
	        required: true,
	        extension: "jpg|jpeg|png|gif"
	       }
	    }
	});


    
</script>