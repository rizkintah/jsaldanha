<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/facilities/edit_sub_cat_fac_process/<?=$detail['id']?>" method="POST" class="form-horizontal" id="frm-add-course">
			<div class="form-group">
				<label for="fn" class="col-sm-2 control-label">Facility Name</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<input type="text" name="fac_name" class="form-control" id="fn" placeholder="Type course name..." value="<?=$detail['fac_name']?>"  required>
				</div>
			</div>
			<div class="form-group">
				<label for="fn" class="col-sm-2 control-label">Facility Category</label>
				<div class="col-sm-9">
					<select name="parent_id" id="inputParent_id" class="form-control" required="required">
						<option value="" selected disabled>Select Facility Category</option>
						<?php  
							foreach ($listFac->result() as $key => $value) {
								?>
									<option value="<?=$value->id?>" <?php if($detail['parent_id'] == $value->id){echo "selected";} ?>><?=$value->fac_name?></option>
								<?php
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="teks">
				<div class="form-group">
					<label for="ckEditor" class="col-sm-2 control-label">Facility Description</label>
					<div class="col-sm-9">
					  	<textarea name="fac_desc" id="ckEditor" class="form-control" rows="3" required placeholder="Type facility description..."><?=$detail['fac_desc']?></textarea>
					</div>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/facilities/manage_main_facilities" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Save Category Facility</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
    //
    $("#frm-add-course").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      fac_desc: {
	        htmlEditor: true
	       }
	    }
	});

	 // select2 department
	$("#sdept").select2(  {
        placeholder: "Select Department",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_dept_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.department_name };
                    })
                };
            }
        },
    });

	
    
</script>