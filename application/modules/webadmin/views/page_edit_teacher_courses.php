<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/courses/edit_teacher_course_process/<?=$detail['id']?>" method="POST" class="form-horizontal" id="frm-add-course">
			<div class="form-group">
    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				<label for="steacher" class="col-sm-2 control-label">Teacher Name</label>
				<div class="col-sm-9">
					<select name="teacher_id" id="steacher" class="form-control" style="width:100%;" required="required">
                        <option value="<?=$detail['teacher_id']?>"><?=$detail['teacher_id']?> - <?=$detail['teacher_name']?></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="scourse" class="col-sm-2 control-label">Course Name</label>
				<div class="col-sm-9">
				  	<select name="courses_id" id="scourse" class="form-control" style="width:100%;" required="required">
                        <option value="<?=$detail['courses_id']?>"><?=$detail['courses_name']?> - <?=$detail['department_name']?></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="sclass" class="col-sm-2 control-label">Class</label>
				<div class="col-sm-9">
					<select name="class_id" id="sclass" class="form-control" style="width:100%;" required>
                        <option value="<?=$detail['class_id']?>"><?=$detail['class_name']?> - <?=$detail['class_desc']?></option>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/courses/teacher_courses" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Save Data</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="javascript:void(0)" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="javascript:void(0)" data-tooltip="true" title="Delete Data">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    }

    //
    $("#frm-add-course").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       }
	    }
	});

	 // select2 department
	$("#scourse").select2(  {
        placeholder: "Type course name",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/courses/get_list_courses_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.courses_name+' - '+obj.department_name };
                    })
                };
            }
        },
    });

    $("#steacher").select2(  {
        placeholder: "Type teacher ID or teache name",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/courses/get_list_teacher_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.teacher_nik, text: obj.teacher_nik+' - '+obj.teacher_name };
                    })
                };
            }
        },
    });

    $("#sclass").select2(  {
        placeholder: "Select Class",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_class_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.class_name+' - '+obj.class_desc };
                    })
                };
            }
        },
    });

	
    
</script>