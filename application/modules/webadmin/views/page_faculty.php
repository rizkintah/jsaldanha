<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title">Management Faculty</h3>
      </div>

      <div class="panel-body">
      		<?=$this->session->flashdata('alertFlash')?>
			<div id="toolbar">
				<a href="<?=base_url()?>webadmin/faculty/add_faculty" title="Add Faculty" class="btn btn-default btn-sm">
					<i class="glyphicon glyphicon-plus"></i> Add Data Faculty
				</a>
			</div>
        	<table 	data-toggle="table"
        		data-toolbar="#toolbar"
	           	--data-search="true"
	           	data-show-refresh="true"
	           	data-show-toggle="true"
	           	data-show-columns="true"
	           	data-show-export="true"
	           	data-filter-control="true"
           		--data-filter-show-clear="true"
	           	--data-show-pagination-switch="true"
	           	data-pagination="true"
	           	data-page-size="20"
			   	data-side-pagination="server" 
			   	data-url="<?=base_url()?>webadmin/faculty/get_list_faculty_ajax"
	           	data-page-list="[20, 35, 50, 100]"
	           	class="table-striped table-hover">
			    <thead>
			        <tr>
			        	<th data-field="rownum" class="col-md-1" >No</th>
		                <th data-field="faculty_name" data-sortable="true" data-filter-control="input">Faculty</th>
		                <th data-field="operate" data-formatter="operateFormatter" data-align="center" class="col-md-1">Action</th>
			        </tr>
			    </thead>
			</table>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="faculty/edit_faculty/'+row.id+'" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="faculty/delete_faculty/'+row.id+'" data-tooltip="true" title="Delete Data" onclick="return confirm(\'are sure want to delete this data?\')">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    };
	
</script>