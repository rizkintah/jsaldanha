<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/manage_pages/edit_admission_process/<?=$this->uri->segment(4)?>" method="POST" class="form-horizontal" id="frm-add-faculty">
			<div class="form-group">
				<label for="fn" class="col-sm-2 control-label">Title</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<input type="text" name="title" class="form-control" id="fn" placeholder="Type Title..." value="<?=$detail['title_en']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="de" class="col-sm-2 control-label">Event Date</label>
				<div class="col-sm-9">
				  	<input type="text" name="event_date" class="form-control datePick" id="de" placeholder="Type Date event..." value="<?=date('m/d/Y',strtotime($detail['date_en']))?>" required>
				</div>
			</div>
			<div class="teks">
			<div class="form-group">
				<label for="ckEditor" class="col-sm-2 control-label">Description</label>
				<div class="col-sm-9">
				  <textarea name="description" id="ckEditor" class="form-control" rows="3" required placeholder="Type news and event description..."><?=$detail['content_en']?></textarea>
				</div>
			</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/manage_pages/admissions" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Save Admission</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="javascript:void(0)" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="javascript:void(0)" data-tooltip="true" title="Delete Data">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    }

    //
    $("#frm-add-faculty").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       }
	    }
	});


    
</script>