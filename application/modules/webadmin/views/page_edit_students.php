<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/students/edit_students_process/<?=$detail['student_id']?>" method="POST" class="form-horizontal" id="frm-add-student" enctype="multipart/form-data">
			<div class="form-group">
				<label for="sid" class="col-sm-2 control-label">Student ID</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<input type="text" name="old_id_student" class="form-control" id="sid" placeholder="Type Student ID..." value="<?=$detail['student_id']?>">
				</div>
			</div>
			<div class="form-group">
				<label for="pid" class="col-sm-2 control-label">Faculty Program</label>
				<div class="col-sm-9">
					<select name="program_id" id="pid" class="form-control" required="required">
						<option value="" selected disabled>Select Program</option>
						<option value="1" <?php if($detail['program_id']==1){echo "selected";} ?>>Undergraduate</option>
						<option value="2" <?php if($detail['program_id']==2){echo "selected";} ?>>Magister / Msc</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="sfac" class="col-sm-2 control-label">Faculty</label>
				<div class="col-sm-9">
					<select name="faculty_id" id="sfac" class="form-control" style="width:100%;" required="required">
						<option value="<?=$detail['faculty_id']?>"><?=$detail['faculty_name']?></option>
					</select>
				</div>
			</div>
			<?php  
				if ($detail['program_url']<>"magister") {
				?>
					<div class="form-group">
						<label for="sdept" class="col-sm-2 control-label">Department</label>
						<div class="col-sm-9">
							<select name="department_id" id="sdept" class="form-control" style="width:100%;" required="required">
								<option value="<?=$detail['department_id']?>"><?=$detail['department_name']?></option>
							</select>
						</div>
					</div>
				<?php
				}
			?>
			<div class="form-group">
				<label for="sname" class="col-sm-2 control-label">Student Status</label>
				<div class="col-sm-9">
					<select name="student_status" id="inputStudent_status" class="form-control" required="required">
						<option value="">Select Student Status</option>
						<option value="Active" <?php if($detail['student_status']=="Active") {echo 'selected';} ?>>Active</option>
						<option value="Inactive" <?php if($detail['student_status']=="Inactive") {echo 'selected';} ?>>Inactive</option>
						<option value="New Student" <?php if($detail['student_status']=="New Student") {echo 'selected';} ?>>New Student</option>
						<option value="Graduate" <?php if($detail['student_status']=="Graduate") {echo 'selected';} ?>>Graduate</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="say" class="col-sm-2 control-label">Academic Year</label>
				<div class="col-sm-9">
				  	<input type="text" name="academic_year" class="form-control" id="say" value="<?=$detail['academic_year']?>" placeholder="Type academic year...(ex : 2015)" required>
				</div>
			</div>
			<div class="form-group">
				<label for="sname" class="col-sm-2 control-label">Student Name</label>
				<div class="col-sm-9">
				  	<input type="text" name="student_name" class="form-control" id="sname" placeholder="Type Student Name..." value="<?=$detail['student_name']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="gender" class="col-sm-2 control-label">Gender</label>
				<div class="col-sm-9">
					<select name="gender" id="gender" class="form-control" required="required">
						<option value="" selected disabled>Select Gender</option>
						<option value="1" <?php if($detail['gender']=="Men"){echo "selected";} ?>>Men</option>
						<option value="2" <?php if($detail['gender']=="Women"){echo "selected";} ?>>Women</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="spass" class="col-sm-2 control-label">Student Password</label>
				<div class="col-sm-9">
				  	<input type="password" name="password" class="form-control" id="spass" placeholder="Type Password..." >
				</div>
			</div>
			<div class="form-group">
				<label  class="col-sm-2 control-label">Student Birthday</label>
				<div class="col-sm-3">
					<div class="input-group">
						<div class="input-group-addon">Place</div>
				  		<input type="text" name="student_birth_place" class="form-control" placeholder="Place of birthday..." value="<?=$detail['student_birth_place']?>" required>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="input-group">
						<div class="input-group-addon">Birth date</div>
				  		<input type="text" name="student_birthday" id="sd" class="form-control" placeholder="Date of birthday..." value="<?=$detail['student_birthday']?>" required>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="sclass" class="col-sm-2 control-label">Student Class</label>
				<div class="col-sm-9">
					<select name="class_id" id="sclass" class="form-control" style="width:100%;" required>
						<option value="<?=$detail['class_id']?>"><?=$detail['class_name']?> - <?=$detail['class_desc']?></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="sphoto" class="col-sm-2 control-label">Student Photo</label>
				<div class="col-sm-5">
				  	<input type="file" name="student_photo" id="sphoto" class="form-control">
				</div>
				<div class="col-sm-4">
					<div class="input-group">
						<input type="text" class="form-control" value="<?=$detail['student_photo']?>" disabled>
						<span class="input-group-btn">
						 	<a href="<?=base_url()?>_media/_students/<?=$detail['student_photo']?>" class="zoom btn btn-primary" target="_blank">
		                    	Click For Zoom
		                    </a>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Student Phone</label>
				<div class="col-sm-9">
				  	<input type="text" name="student_phone" class="form-control" placeholder="Type student phone..." value="<?=$detail['student_phone']?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Student HP</label>
				<div class="col-sm-9">
				  	<input type="text" name="student_mobile" class="form-control" placeholder="Type student handpone..." value="<?=$detail['student_mobile']?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Student Email</label>
				<div class="col-sm-9">
				  	<input type="email" name="student_email" class="form-control" placeholder="Type student email..." value="<?=$detail['student_email']?>" required>
				</div>
			</div>
			<div class="form-group">
				<label for="ckEditor" class="col-sm-2 control-label">Student Address</label>
				<div class="col-sm-9">
				  <textarea name="student_addr" class="form-control" rows="3" required placeholder="Type student address..."><?=$detail['student_addr']?></textarea>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/students" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Save Edit Student</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">

	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="javascript:void(0)" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="javascript:void(0)" data-tooltip="true" title="Delete Data">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    }

    //
    $("#frm-add-student").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       },
	       academic_year:{
	       	required :true,
	       	number:true
	       }
	    },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
	});

     // select2 department
	$("#sfac").select2(  {
        placeholder: "Select Faculty",
        allowClear: true,
        // minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_fac_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.faculty_name };
                    })
                };
            }
        },
    });

    // select2 department
	$("#sdept").select2(  {
        placeholder: "Select Department",
        allowClear: true,
        // minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_dept_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.department_name };
                    })
                };
            }
        },
    });

     // select2 class
	$("#sclass").select2(  {
        placeholder: "Select Student Class",
        allowClear: true,
        // minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_class_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.class_name+' - '+obj.class_desc };
                    })
                };
            }
        },
    });

     // tanggl
    $('#sd').datepicker({
        format: "dd/mm/yyyy",
        forceParse: false,
        autoclose: true
    });


    
</script>
