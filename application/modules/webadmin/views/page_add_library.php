<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/library/add_lib_process" method="POST" class="form-horizontal" id="frm-add-faculty" enctype="multipart/form-data">
			<div class="form-group">
				<label for="fn" class="col-sm-2 control-label">Title</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<input type="text" name="lib_title" class="form-control" id="fn" placeholder="Type Title..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="sdept" class="col-sm-2 control-label">Department</label>
				<div class="col-sm-9">
					<select name="department_id" id="sdept" class="form-control" style="width:100%;" required="required">
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="desc" class="col-sm-2 control-label">Description</label>
				<div class="col-sm-9">
					<textarea name="lib_desc" id="desc" class="form-control" rows="4" required="required" placeholder="Description..."></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="fn" class="col-sm-2 control-label">File Attachment</label>
				<div class="col-sm-9">
				  	<input type="file" name="lib_file" class="form-control" id="fn" placeholder="Type Title..." required>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/library" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Save Library</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
    $("#frm-add-faculty").validate({
    	ignore: [],  
	    rules: {
	      title: {
	      	required: true
	      },
	      img_filename: {
	        required: true,
	        extension: "jpg|jpeg|png"
	       }
	    }
	});

	$("#sdept").select2(  {
        placeholder: "Select Department",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_dept_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.department_name };
                    })
                };
            }
        },
    });


    
</script>