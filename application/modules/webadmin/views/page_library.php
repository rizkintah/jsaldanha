<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      	<div class="panel-body">
	      	<?=$this->session->flashdata('alertFlash')?>
			<div id="toolbar">
				<a href="<?=base_url()?>webadmin/library/add_library" title="Add Image" class="btn btn-default btn-sm">
					<i class="glyphicon glyphicon-plus"></i> Add Library
				</a>
			</div>
	    	<table 	data-toggle="table"
	    		    data-toolbar="#toolbar"
		           	--data-search="true"
		           	data-show-refresh="true"
		           	data-show-toggle="true"
		           	data-show-columns="true"
		           	data-show-export="true"
		           	data-filter-control="true"
		       		--data-filter-show-clear="true"
		           	--data-show-pagination-switch="true"
		           	data-pagination="true"
		           	data-page-size="20"
			   	    data-side-pagination="server" 
			   	    data-url="<?=base_url()?>webadmin/library/get_list_lib_ajax"
		           	data-page-list="[20, 35, 50, 100]"
		           	class="table-striped table-hover">
			    <thead>
			        <tr>
			        	<th data-field="no" class="col-md-1" data-width="2%">No</th>
			        	<th data-field="lib_title" data-filter-control="input" data-sortable="true" data-width="40%" data-cell-style="cellStyle">Title</th>
		                <th data-field="department_name" data-sortable="true" data-filter-control="input" data-width="30%" data-cell-style="cellStyle">department_name</th>
		                <th data-field="lib_desc" data-sortable="true" data-filter-control="input" data-width="30%" data-cell-style="cellStyle">description</th>
		                <th data-field="operate" data-formatter="operateFormatter" data-align="center" class="col-md-1" data-cell-style="cellStyle">Action</th>
			        </tr>
			    </thead>
			</table>
      	</div>
    </div>
<?php include 'page_footer.php'; ?>
<script type="text/javascript">
	var url = '<?=base_url()?>';
	function imgFormatter(value, row, index) {
		return "<img src='"+url+"_media/galleries/"+row.img_filename+"' width='50%' />";
    }

    function operateFormatter(value, row, index) {
    	console.log(row);
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="library/edit_library/'+row.id+'" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="library/delete_library/'+row.id+'" data-tooltip="true" title="Delete Data" onclick="return confirm(\'are you sure want to delete this data?\')">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    };

     function cellStyle(value, row, index, field) {
      return {
        classes: 'text-nowrap another-class'
      };
    }
</script>