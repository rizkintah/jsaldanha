<!DOCTYPE html>
<html lang="en">
<head>
	<title><?=$title?></title>
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link href="<?=base_url()?>_assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/bootflat.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/login.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/plugins/nprogress/nprogress.css" rel="stylesheet" media="screen">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="logo">
        <div class="container">
            <img src="<?=base_url()?>_assets/img/header.png" alt="jsu">
        </div>
    </div>   

    <div class="container">
    	<div class="frm-wrap">
	    	<form method="POST" role="form" class="frm-login" id="frmLogin">
		    	<div class="frm-header">
	    			<h5>LOGIN FORM</h5>
	    		</div>
	    		<div class="frm-conten">
	    			<div class="form-group">
	    				<input type="hidden" class="form-control" value="webadmin" id="url_id">
	    				<input type="hidden" class="form-control" value="<?=$this->security->get_csrf_hash()?>" id="code_id">
	    				<label for="un">Username</label>
	    				<input type="text" name="username" class="form-control" id="un" placeholder="Enter Username" required>
	    			</div>
	    			<div class="form-group">
	    				<label for="pw">Password</label>
	    				<input type="password" name="password" class="form-control" id="pw" placeholder="Enter Password" required>
	    			</div>
	    			<div class="form-group">
				      	<a href="" title="">Forgot password?</a>
				  	</div>
	    		
	    			<button type="submit" class="btn btn-dark-grey btn-block" id="btn">LOGIN</button>
	    		</div>
    		</form>
    	</div>
    </div>

    <script type="text/javascript">
    	var direct = "<?=base_url()?>";
    </script>
    <script src="<?=base_url()?>_assets/js/jquery-1.12.3.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/modernizr-custom.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/jquery_validation/jquery.validate.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/jquery_validation/additional-methods.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/nprogress/nprogress.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/login.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>