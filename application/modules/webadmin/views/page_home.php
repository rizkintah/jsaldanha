<?php include 'page_header.php'; ?>
	<!-- <img src="<?=base_url()?>_assets/img/welcome.png" alt="welcome" class="img-responsive img-welcome"> -->

	<div class="flex-area">
		<div style="width:600px;border: 1px solid #01B302;border-radius: 10px;padding: 20px;">
			<div class="welcome-text">
				<h2><span>WELCOME TO<br class="hidden"></span> Joao Saldanha University</h2>
				<h4>"Southeast's Global University"</h4>
			</div>
		</div>
	</div>
<?php include 'page_footer.php'; ?>


<style type="text/css">
	.flex-area {
		display: flex;
    justify-content: center;
    align-items: center;  
    margin: 10vh auto;
	}

	@media screen and (max-width: 390px) {
	  .welcome-text h2 {
			font-size: 25px;
		}
			.welcome-text span {
				font-size: 14px;
			}

		.welcome-text h4 {
			font-size: 16px;
		}

		.hidden {
			display: block !important;
		}
	}

	/*@media screen and (max-width: 992) { 
		.welcome-text h2 {
			font-size: 10px;
			color: red !important;
		}
			.welcome-text span {
				font-size: 14px;
			}

		.welcome-text h4 {
			font-size: 16px;
		}

		.hidden {
			display: block !important;
		}
	}*/
</style>