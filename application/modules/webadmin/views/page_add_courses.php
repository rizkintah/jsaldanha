<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title"><?=$title?></h3>
      </div>

      <div class="panel-body">
        <form  action="<?=base_url()?>webadmin/courses/add_course_process" method="POST" class="form-horizontal" id="frm-add-course">
			<div class="form-group">
				<label for="fn" class="col-sm-2 control-label">Course Name</label>
				<div class="col-sm-9">
	    			<input type="hidden" class="form-control" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>">
				  	<input type="text" name="courses_name" class="form-control" id="fn" placeholder="Type course name..." required>
				</div>
			</div>
			<div class="form-group">
				<label for="sdept" class="col-sm-2 control-label">Department</label>
				<div class="col-sm-9">
					<select name="department_id" id="sdept" class="form-control" style="width:100%;" required="required">
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="grade" class="col-sm-2 control-label">Course Grade</label>
				<div class="col-sm-9">
					<select name="grade_id" id="gradeSelect" class="form-control" style="width:100%;" required>
						<option  disabled selected>Select Grade</option>
						<?php
							foreach ($gradeList->result() as $grades) {
								?>
									<option value="<?=$grades->smester_name?>"><?=$grades->smester_name?></option>
								<?php
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="unit" class="col-sm-2 control-label">Course Unit</label>
				<div class="col-sm-9">
					<input type="text" name="courses_sks" id="unit" class="form-control" placeholder="Type course unit..." required="required">
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
				  <a href="<?=base_url()?>webadmin/courses/manage_courses" class="btn btn-danger">Cancel</a>
				  <button type="submit" class="btn btn-primary">Add Course</button>
				</div>
			</div>
		</form>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	var $table = $('#table'),
		$del_selected = 	$("#del_selected");

	function operateFormatter(value, row, index) {
        return [
            '<a class="btn btn-xs btn-primary edit_btn" href="javascript:void(0)" data-tooltip="true" title="Edit Data">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>  ',
            '<a class="btn btn-xs btn-danger remove" href="javascript:void(0)" data-tooltip="true" title="Delete Data">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
        ].join('');
    }

    //
    $("#frm-add-course").validate({
    	ignore: [],  
	    rules: {
	      faculty_name: {
	      	required: true
	      },
	      description: {
	        htmlEditor: true
	       }
	    }
	});

	 // select2 department
	$("#sdept").select2(  {
        placeholder: "Select Department",
        allowClear: true,
        minimumInputLength: 2,
        ajax: {
            // The number of milliseconds to wait for the user to stop typing before issuing the ajax request
            delay: 400,
            url: "<?=base_url()?>webadmin/students/get_list_dept_json",
            dataType: "json",
            // cache: "true",
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.department_name };
                    })
                };
            }
        },
    });

	
    
</script>