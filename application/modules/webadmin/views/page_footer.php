
	</div>


	<footer class="footer">
      <div class="container">
   		<h5 class="text-muted">"Local University, Global Responsibility"</h5>
        <ul class="nav nav-pills nav-footer hidden-xs hidden-sm">
            <li>
                <a href="#" target="_blank"><img src="<?=base_url()?>_assets/img/fb-60.png" alt="Facebook"></a>
            </li>
            <li>
                <a href="#" target="_blank"><img src="<?=base_url()?>_assets/img/twitter-60.png" alt="Twitter"></a>
            </li>
        </ul>
      </div>
    </footer>

	<script src="<?=base_url()?>_assets/js/jquery-1.12.3.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/modernizr-custom.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/jquery.ajaxfileupload.js" type="text/javascript" charset="utf-8"></script>

	<script src="<?=base_url()?>_assets/plugins/bootstrap-table/src/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/bootstrap-table/src/extensions/filter-control/bootstrap-table-filter-control.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="<?=base_url()?>_assets/plugins/bs-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript" charset="utf-8"></script>

	<script src="<?=base_url()?>_assets/plugins/nprogress/nprogress.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/jquery_validation/jquery.validate.min.js" type="text/javascript" ></script>
	<script src="<?=base_url()?>_assets/plugins/jquery_validation/additional-methods.min.js" type="text/javascript" ></script>
	<script src="<?=base_url()?>_assets/plugins/select2/js/select2.full.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/ekko-lightbox_2.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			CKEDITOR.replace('ckEditor');
		})
		
		$(window).scroll(function() {
		  if ($(document).scrollTop() > 50) {
		    $('#header').addClass('to_fixed');
		  } else {
		    $('#header').removeClass('to_fixed');
		  }
		});
		var direct = "<?=base_url()?>";
		
		// untuk ckeditor
		jQuery.validator.addMethod("htmlEditor", function (value, element) {
			var _editor = CKEDITOR.instances[element.id];
			_editor.updateElement();
			var elValue = $(element).val();
			if (elValue.length > 0) {
				return true;
			}
			return false;
		}, "This field is required.");

		$.validator.setDefaults({
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');

	            var elem = $(element);
	            
	            if(elem.hasClass('s-select2')) {
	                var isMulti = (!!elem.attr('multiple')) ? 's' : '';
	                elem.siblings('.select2form').find('.select2-choice'+isMulti+'').addClass('has-error');            
	            } else {
	                elem.addClass('has-error');
	            }
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		    	// console.log(element.parent());
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } 
		        else {
		            error.insertAfter(element);
		        }

		        if (element.hasClass("teks")) {
					error.insertAfter("#cke_ckEditor");
				}
				else {
					error.insertAfter(element);
				}
		       
		    }
		});
		
		$('.datePick').datepicker({
		    todayBtn: "linked",
		    autoclose: true,
		    todayHighlight: true
		});
	</script>
</body>
</html>