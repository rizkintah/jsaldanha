<?php include 'page_header.php'; ?>
	<br />
	<div class="panel panel-dark-grey">
      <div class="panel-heading">
        <h3 class="panel-title">View Detail Online Registration</h3>
      </div>

      <div class="panel-body">
      		<div style="overflow:hidden;">
  				<button type="button" onclick="printDiv('areaPrint')" class="btn btn-primary btn-sm pull-right">
  					<i class="glyphicon glyphicon-print"></i> Print Form Admission
  				</button>
      		</div>
  			<hr>
  			<div id="areaPrint" style="position:relative;width:80%;margin:0 auto;">
  				 <style media="print" type="text/css">
			        #header, #footer {display:none;}
			        @page{
					  size: auto;
					}
			    </style>

	  				<div style="text-align:center;border-bottom:2px solid #b86333;padding-bottom:3px;">
		  				<img src="<?=base_url()?>_assets/img/headerForm2.png" alt="" width="70%">
	  				</div>
	  				<div style="text-align:center;position:relative;">
	  					<h4><u>REGISTRATION FORM</u></h4>
	  					<h5><?=strtoupper($detail['program_name'])?></h5>
	  					<img src="<?=base_url()?>_media/_students/<?=$detail['student_photo']?>"
	  						 style="position:absolute;right:0;top:0;width:118px;height:157px;">
	  				</div>
	  				<div style="width:90%;clear:both;float:left;">
	  					<label style="width:200px;float:left">Registration Number for Office only</label>
	  					<input type="text" value=""
	  							style="border:1px solid #000;
	  								   margin-left:5px;
	  								   float:left;
	  								   width:53%;
	  								   padding:10px;">	
	  				</div>
	  				<div style="width:90%;clear:both;float:left;margin-top:10px;">
		  					<label style="width:200px;float:left;">Faculty</label>
		  					<input readonly value="<?=ucwords($detail['faculty_name'])?>"
		  							style="border:1px solid #000;
		  								   margin-left:5px;
		  								   float:left;
		  								   width:53%;
		  								   padding:10px;">	
		  				</div>
	  				<?php  
	  					if ($detail['program_name']=="Undergraduate") {
	  						?>
	  							<div style="width:100%;clear:both;float:left;margin-top:10px;">
				  					<label style="width:200px;float:left;">Department</label>
				  					<input readonly value="<?=ucwords($detail['department_name'])?>"
				  							style="border:1px solid #000;
				  								   margin-left:5px;
				  								   float:left;
				  								   width:65%;
				  								   padding:10px;">	
				  				</div>
	  						<?php
	  					}
	  				?>
	  				<div style="width:100%;clear:both;float:left;margin-top:10px;">
	  					<label style="width:200px;float:left;">Complete Name</label>
	  					<input readonly value="<?=ucwords($detail['student_name'])?>"
	  							style="border:1px solid #000;
	  								   margin-left:5px;
	  								   float:left;
	  								   width:65%;
	  								   padding:10px;">	
	  				</div>
	  				<div style="width:100%;clear:both;float:left;margin-top:10px;">
	  					<label style="width:200px;float:left;">Gender</label>
	  					<input readonly value="<?=ucwords($detail['gender'])?>"
	  							style="border:1px solid #000;
	  								   margin-left:5px;
	  								   float:left;
	  								   width:65%;
	  								   padding:10px;">	
	  				</div>
	  				<div style="width:100%;clear:both;float:left;margin-top:10px;">
	  					<label style="width:200px;float:left;">Place of Birth</label>
	  					<input readonly value="<?=ucwords($detail['student_birth_place'])?>"
	  							style="border:1px solid #000;
	  								   margin-left:5px;
	  								   float:left;
	  								   width:65%;
	  								   padding:10px;">	
	  				</div>
	  				<div style="width:100%;clear:both;float:left;margin-top:10px;">
	  					<label style="width:200px;float:left;">Date of Birth</label>
	  					<input readonly value="<?=ucwords($detail['student_birthday'])?>"
	  							style="border:1px solid #000;
	  								   margin-left:5px;
	  								   float:left;
	  								   width:65%;
	  								   padding:10px;">	
	  				</div>
	  				<div style="width:100%;clear:both;float:left;margin-top:10px;">
	  					<label style="width:200px;float:left;">Address</label>
	  					<textarea readonly rows="3" style="border:1px solid #000;
		  								   margin-left:5px;
		  								   float:left;
		  								   width:65%;
		  								   padding:10px;
		  								   resize:none;"><?=$detail['student_addr']?></textarea>
	  					
	  				</div>
	  				<div style="width:100%;clear:both;float:left;margin-top:10px;">
	  					<label style="width:200px;float:left;">Contact Number</label>
	  					<div style="float:left;width:240px">
		  					<input readonly value="<?=$detail['student_mobile']?>"
		  							style="border:1px solid #000;
		  								   margin-left:5px;
		  								   float:left;
		  								   width:150px;
		  								   padding:10px;">
		  					<label style="width:50px;float:left;margin-left:5px;margin-top:10px;">Mobile</label>
	  					</div>
	  					<div style="float:left;width:250px">
		  					<input readonly value="<?=$detail['student_phone']?>"
		  							style="border:1px solid #000;
		  								   float:left;
		  								   width:140px;
		  								   padding:10px;">
		  					<label style="width:80px;float:left;margin-left:5px;">Phone (Land Line)</label>
	  					</div>
	  				</div>
	  				<div style="width:100%;clear:both;float:left;margin-top:10px;">
	  					<label style="width:200px;float:left;">Email</label>
	  					<input readonly value="<?=$detail['student_email']?>"
	  							style="border:1px solid #000;
	  								   margin-left:5px;
	  								   float:left;
	  								   width:65%;
	  								   padding:10px;">	
	  				</div>
	  				<div style="float:left;width:100%;background:;margin-top:50px;">
	  					<div style="float:left;border:1px solid #F78513;padding:5px 10px;margin-top:20px;">
	  						NB : no refundable
	  					</div>
	  					<div style="float:right;width:30%;text-align:center;background:;">
	  						<span>
	  							Date, <input type="text" value=".......... / .............. / 2017"
	  										 style="border:none;outline:none;width:65%">
	  						</span>
	  						<input type="text" value="<?=ucwords($detail['student_name'])?>"
	  								style="font-size:13px;width:100%;
	  								text-align:center;
	  								margin-top:80px;
	  								border-top:1px solid #000;
	  								border-right:none;
	  								border-left:none;
	  								border-bottom:none;
	  								outline:0;">	
	  					</div>
	  				</div>
	  		</div>
      </div>
    </div>
<?php include 'page_footer.php'; ?>

<script type="text/javascript">
	function printDiv(divName) {
          var printContents = document.getElementById(divName).innerHTML;
          var originalContents = document.body.innerHTML;
          document.body.innerHTML = printContents;

          window.print();


          document.body.innerHTML = originalContents;
    }
</script>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>