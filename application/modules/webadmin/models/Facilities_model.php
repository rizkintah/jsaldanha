<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities_model extends CI_Model {

	function get_list_main_fac_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										SELECT *
										FROM jsuuniv_facilities
										WHERE parent_id =0
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM jsuuniv_facilities
									WHERE 1=1
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}

	function insert_to_db($data=array())
	{
		return $this->db->insert('_facilities', $data);
	}

	function update_to_db($data=array(),$where=array())
	{	
		$this->db->where($where);
		return $this->db->update('_facilities', $data);
	}

	function get_facilities_detail($id)
	{	
		$this->db->where('id', $id);
		$query = $this->db->get('_facilities');

		return $query;
	}	

	function delete_fac($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('_facilities');
	}

	// sub cat facility
	function get_list_sub_fac_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										SELECT A.*, B.fac_name parent_name
										FROM jsuuniv_facilities A, jsuuniv_facilities B
										WHERE A.parent_id = B.id
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM jsuuniv_facilities A, jsuuniv_facilities B
									WHERE A.parent_id = B.id
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}

	function get_list_main_cat()
	{
		$this->db->where('parent_id', 0);
		return $this->db->get('_facilities');
	}

}

/* End of file Facilities_model.php */
/* Location: ./application/modules/webadmin/models/Facilities_model.php */