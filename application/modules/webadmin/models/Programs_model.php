<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Programs_model extends CI_Model {

	function insert_programs_to_db($data=array())
	{
		$query = $this->db->insert('_programs', $data);		
		return $query;
	}
	
	function update_programs_to_db($data=array(),$where = array())
	{
		$query = $this->db->update('_programs', $data,$where);		
		return $query;
	}	

	function delete_programs_from_db($where = array())
	{
		$query = $this->db->delete('_programs', $where);		
		return $query;
	}	

	function get_list_program() {
		$this->db->order_by('id', 'asc');
		return $this->db->get('_programs')->result_array();
	}

	function get_data_program($id) {
		$this->db->where('id', $id);
		return $this->db->get('_programs')->row_array();
	}

	function get_list_programs_ajax($order,$limit,$offset,$qSearch)
	{
		$fields = $this->db->list_fields('_programs');
		$sQuery="";
		if (!empty($qSearch)) {
			foreach ($fields as $idx => $key ) {
				if ($idx == 0) {
					$sQuery .= " UPPER(".$key.") LIKE UPPER('%$qSearch%') ";
				}else{
					$sQuery .= " OR UPPER(".$key.") LIKE UPPER('%$qSearch%') ";
				}
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}
		

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT *
									  FROM {TBL_PRE}_programs
									  WHERE 1=1
									  $sQuery
									  $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );

        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_programs
                                      WHERE 1=1
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}

}

/* End of file Programs_model.php */
/* Location: ./application/modules/webadmin/models/Programs_model.php */