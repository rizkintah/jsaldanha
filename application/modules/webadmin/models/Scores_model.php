<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scores_model extends CI_Model {

	function get_list_students_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										SELECT A.student_id, A.student_name, B.department_name,A.student_birth_place,
											   A.student_birthday, C.faculty_name, D.class_name, A.student_email, 
											   A.student_phone, A.student_mobile, A.student_addr, A.student_status
										FROM {TBL_PRE}_student A, {TBL_PRE}_department B, {TBL_PRE}_faculty C, {TBL_PRE}_class D
										WHERE A.department_id = B.id
										-- AND B.faculty_id = C.id
										AND A.class_id = D.id
										AND A.student_status='Active'
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM {TBL_PRE}_student A, {TBL_PRE}_department B, {TBL_PRE}_faculty C, {TBL_PRE}_class D
									WHERE A.department_id = B.id
									-- AND B.faculty_id = C.id
									AND A.class_id = D.id
									AND A.student_status='Active'
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}	

	function insert_to_db($data=array())
	{
		$query = $this->db->insert('_score_file', $data);		
		return $query;
	}	

	function get_list_score($student_id)
	{
		$this->db->where('student_id', $student_id);
		return $this->db->get('_score_file');
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$delFile = $this->db->get('_score_file')->row_array();
		@unlink('./_media/_var/'.$delFile['file_score']);

		$this->db->where('id', $id);
		$query = $this->db->delete('_score_file');
		return $query;
	}

}

/* End of file Scores_model.php */
/* Location: ./application/modules/webadmin/models/Scores_model.php */