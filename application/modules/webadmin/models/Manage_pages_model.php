<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_pages_model extends CI_Model {

	function get_pages_detail($pages)
	{	
		$this->db->where('content_name',$pages);
		$query = $this->db->get('_contents');

		return $query;
	}	

	function update_pages_db($data, $content_name)
	{
		$this->db->where('content_name', $content_name);
		$query = $this->db->update('_contents', $data);
		return $query;
	}

	function insert_event_db($data)
	{
		$query = $this->db->insert('_news_event', $data);
		return $query;
	}	

	function get_list_news_event_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}
		

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT id,url_en,title_en,date_en,fnStripTags(CONCAT(SUBSTRING(content_en,1, 250),' ...')) content_en
									  FROM {TBL_PRE}_news_event
									  WHERE 1=1
									  $sQuery
									  $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );
		// echo  $this->db->last_query();
        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_news_event
                                      WHERE 1=1
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}

	function get_detail_edit_news_event($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('_news_event');
		return $query;
	}

	function update_edit_news_event($id,$data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('_news_event', $data);
		return $query;
	}


	function insert_galleries_db($data)
	{
		$query = $this->db->insert('_galleries',$data);
		return $query;	
	}

	function get_list_galleries_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}
		

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT *
									  FROM {TBL_PRE}_galleries
									  WHERE 1=1
									  $sQuery
									  $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );
		// echo  $this->db->last_query();
        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_galleries
                                      WHERE 1=1
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}

	function delete_galleri($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->delete('_galleries');
		return $query;
	}
	
	// home slide
	function get_list_slides_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}
		

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT *
									  FROM {TBL_PRE}_slides
									  WHERE 1=1
									  $sQuery
									  $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );
		// echo  $this->db->last_query();
        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_slides
                                      WHERE 1=1
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}

	function insert_slides_db($data)
	{
		$query = $this->db->insert('_slides',$data);
		return $query;	
	}

	function delete_slides($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->delete('_slides');
		return $query;
	}


	function insert_admission_db($data)
	{
		$query = $this->db->insert('_admissions', $data);
		return $query;
	}	

	function get_list_admissions_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}
		

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT id,url_en,title_en,date_en,fnStripTags(CONCAT(SUBSTRING(content_en,1, 250),' ...')) content_en
									  FROM {TBL_PRE}_admissions
									  WHERE 1=1
									  $sQuery
									  $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );
		// echo  $this->db->last_query();
        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_admissions
                                      WHERE 1=1
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}

	function get_detail_edit_admissions($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('_admissions');
		return $query;
	}

	function update_edit_admissions($id,$data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('_admissions', $data);
		return $query;
	}

}

/* End of file Manage_pages_model.php */
/* Location: ./application/modules/webadmin/models/Manage_pages_model.php */