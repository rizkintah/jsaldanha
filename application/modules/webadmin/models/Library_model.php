<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library_model extends CI_Model {

	function insert_lib_to_db($data=array())
	{
		return $this->db->insert('_library', $data);
	}	

	function update_lib_db($data=array(),$where = array())
	{
		$this->db->where($where);
		$query = $this->db->update('_library', $data);
		return $query;
	}

	function get_list_lib_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										SELECT A.id, A.lib_title, CONCAT(SUBSTRING(A.lib_desc,1, 150),' ...') lib_desc, B.department_name
										FROM {TBL_PRE}_library A, {TBL_PRE}_department B
										WHERE A.department_id = B.id
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM {TBL_PRE}_library A, {TBL_PRE}_department B
									WHERE A.department_id = B.id
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}

	function get_detail($id)
	{
		$query = $this->db->query(" SELECT A.*, B.department_name
									FROM {TBL_PRE}_library A, {TBL_PRE}_department B
									WHERE A.department_id = B.id
									AND A.id='$id'");
		return $query;
	}

	function delete_lib($id)
	{
		$this->db->where('id', $id);
		$delFile = $this->db->get('_library')->row_array();
		@unlink('./_media/_lib/'.$delFile['lib_file']);

		$this->db->where('id', $id);
		$query = $this->db->delete('_library');
		return $query;
	}

}

/* End of file Library_model.php */
/* Location: ./application/modules/webadmin/models/Library_model.php */