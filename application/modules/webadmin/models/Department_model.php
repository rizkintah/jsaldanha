<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department_model extends CI_Model {
	
	function insert_department_to_db($data=array())
	{
		$query = $this->db->insert('_department', $data);		
		return $query;
	}
	
	function update_department_to_db($data=array(),$where = array())
	{
		$query = $this->db->update('_department', $data,$where);		
		return $query;
	}	

	function delete_department_from_db($where = array())
	{
		$query = $this->db->delete('_department', $where);		
		return $query;
	}	

	function get_list_department_ajax($order,$limit,$offset,$qSearch)
	{
		$fields = $this->db->list_fields('_department');
		$sQuery="";
		if (!empty($qSearch)) {
			foreach ($fields as $idx => $key ) {
				if ($idx == 0) {
					$sQuery .= " UPPER(".$key.") LIKE UPPER('%$qSearch%') ";
				}else{
					$sQuery .= " OR UPPER(".$key.") LIKE UPPER('%$qSearch%') ";
				}
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}
		

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT a.*, b.faculty_name, b.faculty_desc,b.faculty_addr
									  FROM {TBL_PRE}_department a, {TBL_PRE}_faculty b
									  WHERE a.faculty_id = b.id
									  AND department_stat = 'Active'
									  $sQuery
									  $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );

        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_department
                                      WHERE 1=1 AND department_stat = 'Active'
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}

	function get_data_department($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('_department');
		return $query;
	}

	function get_list_faculty()
	{
		$query = $this->db->get('_faculty');
		return $query;
		
	}
}