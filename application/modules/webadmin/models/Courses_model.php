<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_model extends CI_Model {

	function get_list_courses_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										SELECT A.id, A.courses_name, A.grade_id, A.courses_sks, B.department_name, C.faculty_name
										FROM {TBL_PRE}_courses A, {TBL_PRE}_department B, {TBL_PRE}_faculty C
										WHERE A.department_id = B.id
										AND B.faculty_id = C.id
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM {TBL_PRE}_courses A, {TBL_PRE}_department B, {TBL_PRE}_faculty C
									WHERE A.department_id = B.id
									AND B.faculty_id = C.id
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}

	function get_detail($id)
	{
		$query = $this->db->query(" SELECT A.id, A.courses_name, A.department_id, A.grade_id, A.courses_sks, B.department_name
									FROM {TBL_PRE}_courses A, {TBL_PRE}_department B
									WHERE A.department_id = B.id
									AND A.id='$id'");
		return $query;
	}	

	function insert_to_db($data=array())
	{
		return $this->db->insert('_courses', $data);
	}

	function update_to_db($data=array(),$where=array())
	{	
		$this->db->where($where);
		return $this->db->update('_courses', $data);
	}

	function get_list_grade()
	{
		return $this->db->get('_smester');
	}

	function get_list_course()
	{
		return $this->db->get('_courses');
	}

	function delete_course($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('_courses');
	}

# **************** TEACHER COURSE & CLASS METHOD ****************
	function get_list_teacher_courses_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										SELECT A.id,A.teacher_id, B.teacher_name, C.courses_name, D.class_name,D.class_desc,E.department_name
										FROM {TBL_PRE}_teacher_courses A, {TBL_PRE}_teacher B, {TBL_PRE}_courses C, {TBL_PRE}_class D, {TBL_PRE}_department E
										WHERE A.teacher_id = b.teacher_nik
										AND A.courses_id = C.id
										AND A.courses_id = C.id
										AND A.class_id = D.id
										AND C.department_id = E.id
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM {TBL_PRE}_teacher_courses A, {TBL_PRE}_teacher B, {TBL_PRE}_courses C, {TBL_PRE}_class D, {TBL_PRE}_department E
									WHERE A.teacher_id = b.teacher_nik
									AND A.courses_id = C.id
									AND A.courses_id = C.id
									AND A.class_id = D.id
									AND C.department_id = E.id
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}	

	function insert_to_db_teacher_course($data=array())
	{
		return $this->db->insert('_teacher_courses', $data);
	}

	function update_to_teacher_course($data=array(),$where=array())
	{	
		$this->db->where($where);
		return $this->db->update('_teacher_courses', $data);
	}

	function get_detail_teacher_course($id)
	{
		$query = $this->db->query(" SELECT A.id,A.teacher_id, A.class_id, A.courses_id,B.teacher_name, C.courses_name, D.class_name,D.class_desc,E.department_name
									FROM {TBL_PRE}_teacher_courses A, {TBL_PRE}_teacher B, {TBL_PRE}_courses C, {TBL_PRE}_class D, {TBL_PRE}_department E
									WHERE A.teacher_id = b.teacher_nik
									AND A.courses_id = C.id
									AND A.courses_id = C.id
									AND A.class_id = D.id
									AND C.department_id = E.id
									AND A.id ='$id' ");
		return $query;
	}

	function get_list_teacher_json($term)
	{
		$query = $this->db->query("SELECT *
								   FROM {TBL_PRE}_teacher
								   WHERE UPPER(teacher_name) LIKE UPPER('%$term%')
								   OR UPPER(teacher_nik) LIKE UPPER('%$term%')");
		return $query;
	}

	function get_list_courses_json($term)
	{
		$query = $this->db->query("SELECT A.*, B.department_name
								   FROM {TBL_PRE}_courses A, {TBL_PRE}_department B
								   WHERE A.department_id = B.id 
								   AND UPPER(courses_name) LIKE UPPER('%$term%')");
		return $query;
	}

	function delete_teacher_course($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('_teacher_courses');
	}

}

/* End of file Courses_model.php */
/* Location: ./application/modules/webadmin/models/Courses_model.php */