<?php

/* Model Main */

class Faculty_model extends CI_Model {
	
	function insert_faculty_to_db($data=array())
	{
		$query = $this->db->insert('_faculty', $data);		
		return $query;
	}
	
	function update_faculty_to_db($data=array(),$where = array())
	{
		$query = $this->db->update('_faculty', $data,$where);		
		return $query;
	}	

	function delete_faculty_from_db($where = array())
	{
		$query = $this->db->delete('_faculty', $where);		
		return $query;
	}	

	function get_list_faculty_ajax($order,$limit,$offset,$qSearch)
	{
		$fields = $this->db->list_fields('_faculty');
		$sQuery="";
		if (!empty($qSearch)) {
			foreach ($fields as $idx => $key ) {
				if ($idx == 0) {
					$sQuery .= " UPPER(".$key.") LIKE UPPER('%$qSearch%') ";
				}else{
					$sQuery .= " OR UPPER(".$key.") LIKE UPPER('%$qSearch%') ";
				}
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}
		

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT *
									  FROM {TBL_PRE}_faculty
									  WHERE 1=1
									  $sQuery
									  $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );

        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_faculty
                                      WHERE 1=1
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}
	
	function get_data_faculty($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('{TBL_PRE}_faculty');
		
		return $query->row_array();
		
	}

	function get_list_program() {
		$this->db->order_by('id', 'asc');
		return $this->db->get('_programs')->result_array();

	}
}