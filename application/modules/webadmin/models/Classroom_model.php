<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classroom_model extends CI_Model {

	function get_list_classroom_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										SELECT *
										FROM {TBL_PRE}_class
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM {TBL_PRE}_class
									WHERE 1=1
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}

	function insert_to_db($data=array())
	{
		return $this->db->insert('_class', $data);
	}

	function update_to_db($data=array(),$where=array())
	{	
		$this->db->where($where);
		return $this->db->update('_class', $data);
	}

	function get_detail($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('_class');
	}

	function delete_classroom($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('_class');
	}

}

/* End of file Classroom_model.php */
/* Location: ./application/modules/webadmin/models/Classroom_model.php */