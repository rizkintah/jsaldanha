<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students_model extends CI_Model {

	function get_list_students_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$q = "SELECT *
				FROM 
				(
					SELECT A.*, D.id AS departmen_id,D.department_name, B.id AS faculty_id,B.faculty_name, C.program_name,C.program_url,
						   E.class_name, E.class_desc
					FROM jsuuniv_student A LEFT JOIN jsuuniv_faculty B
					ON A.department_id = B.id
					LEFT JOIN jsuuniv_programs C
					ON B.program_id = C.id
					LEFT JOIN jsuuniv_department D
					ON B.id = D.faculty_id
					LEFT JOIN jsuuniv_class E
					ON A.class_id = E.id 
				) A";

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										$q
	                                ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
	                              	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM
									(
										$q
									) A
									WHERE 1=1
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}	
	function get_list_fac_json($term)
	{
		$query = $this->db->query("SELECT *
								   FROM {TBL_PRE}_faculty
								   WHERE UPPER(faculty_name) LIKE UPPER('%$term%')
								   AND faculty_stat='Active'");
		return $query;
	}
	function get_list_dept_json($term)
	{
		$query = $this->db->query("SELECT *
								   FROM {TBL_PRE}_department
								   WHERE UPPER(department_name) LIKE UPPER('%$term%')
								   AND department_stat='Active'");
		return $query;
	}

	function get_list_class_json($term)
	{
		$query = $this->db->query("SELECT *
								   FROM {TBL_PRE}_class
								   WHERE UPPER(class_name) LIKE UPPER('%$term%')");
		return $query;
	}

	function insert_to_db($data=array())
	{
		return $this->db->insert('_student', $data);
	}

	function update_to_db($data=array(),$where=array())
	{	
		$this->db->where($where);
		return $this->db->update('_student', $data);
	}

	function get_detail($student_id)
	{
		$query = $this->db->query(" 	SELECT *
						FROM
						(
							SELECT A.*, D.id AS departmen_id,D.department_name, B.id AS faculty_id,B.faculty_name, C.program_name,C.program_url,
							E.class_name, E.class_desc,C.id as program_id
							FROM jsuuniv_student A LEFT JOIN jsuuniv_faculty B
							ON A.department_id = B.id
							LEFT JOIN jsuuniv_programs C
							ON B.program_id = C.id
							LEFT JOIN jsuuniv_department D
							ON B.id = D.faculty_id
							LEFT JOIN jsuuniv_class E
							ON A.class_id = E.id 
						) A
						WHERE student_id='$student_id'");
		return $query;
	}

	function delete($student_id)
	{
		$this->db->where('student_id', $student_id);
		return $this->db->delete('_student');
	}


	/*************** ONLINE REG ****************************/
	function get_list_students_onreg_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$q = "	SELECT *
				FROM 
				(
					SELECT A.*, B.id AS departmen_id ,B.department_name, C.id AS faculty_id,C.faculty_name, D.program_name,D.program_url
					FROM jsuuniv_student A, jsuuniv_department B, jsuuniv_faculty C, jsuuniv_programs D
					WHERE A.department_id =  B.id
					AND B.faculty_id = C.id
					AND C.program_id = D.id
					UNION
					SELECT A.*, D.id AS departmen_id,D.department_name, B.id AS faculty_id,B.faculty_name, C.program_name,C.program_url
					FROM jsuuniv_student A JOIN jsuuniv_faculty B 
					ON A.department_id = B.id
					JOIN jsuuniv_programs C
					ON B.program_id = C.id
					AND B.program_id=2
					LEFT JOIN jsuuniv_department D
					ON B.id = D.faculty_id
				) A
				WHERE student_status='New Student'";

		$query = $this->db->query("	SELECT @rownum:=@rownum+1 AS no, t1.*
									FROM (
										$q
                                    ) t1, (SELECT @rownum:=0) t2
									WHERE 1=1
									$sQuery
								  	$order
                                  	LIMIT $offset,$limit");

		$q_jml = $this->db->query(" SELECT COUNT(*) jml
									FROM ($q) A
									WHERE 1=1
									$sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$query->result());
		return $data;
	}

	function view_detail_onreg($student_id)
	{
	 	$query = $this->db->query("SELECT *
									FROM 
									(
										SELECT A.*, B.id AS departmen_id ,B.department_name, C.id AS faculty_id,C.faculty_name, D.program_name,D.program_url
										FROM jsuuniv_student A, jsuuniv_department B, jsuuniv_faculty C, jsuuniv_programs D
										WHERE A.department_id =  B.id
										AND B.faculty_id = C.id
										AND C.program_id = D.id
										UNION
										SELECT A.*, D.id AS departmen_id,D.department_name, B.id AS faculty_id,B.faculty_name, C.program_name,C.program_url
										FROM jsuuniv_student A JOIN jsuuniv_faculty B 
										ON A.department_id = B.id
										JOIN jsuuniv_programs C
										ON B.program_id = C.id
										AND B.program_id=2
										LEFT JOIN jsuuniv_department D
										ON B.id = D.faculty_id
									) A
									WHERE student_status='New Student'
									AND student_id='$student_id'");
	 	return $query;
	}	 

}

/* End of file Students_model.php */
/* Location: ./application/modules/webadmin/models/Students_model.php */