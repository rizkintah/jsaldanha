<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers_model extends CI_Model {

	function get_list_teachers_ajax($order,$limit,$offset,$qSearch)
	{
		$sQuery="";
		if (!empty($qSearch)) {
			$i=0;
			foreach ($qSearch as $idx => $key ) {
				if ($i==0) {
					$sQuery .= " UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}else{
					$sQuery .= " OR UPPER(".$idx.") LIKE UPPER('%$key%') ";
				}
				$i++;
			}
		}
		if (!empty($qSearch)) {
			$sQuery = "AND (\n$sQuery)";
		}

		$q_list = $this->db->query("SELECT @rownum:=@rownum+1 AS rownum, t1.*
									FROM (
									  SELECT *
									  FROM {TBL_PRE}_teacher
									  WHERE 1=1 $sQuery $order
                                      LIMIT $offset,$limit
									) t1, (SELECT @rownum:=0) t2"
								  );
		// echo  $this->db->last_query();
        $q_jml = $this->db->query(" SELECT count(*) jml
                                      FROM {TBL_PRE}_teacher
                                      WHERE 1=1
                                      $sQuery")->row_array();
		$data = array('total'=> $q_jml['jml'], 'rows'=>$q_list->result());
		return $data;
	}

	function insert_teacher_db($data=array())
	{
		return $this->db->insert('_teacher', $data);
	}

	function update_teacher_db($data=array(), $where=array())
	{
		$this->db->where($where);
		return $this->db->update('_teacher', $data);
	}	

	function get_detail($id) {
		$this->db->where('teacher_nik', $id);
		return $this->db->get('_teacher');
	}

	function get_delete($id)
	{
		$this->db->where('teacher_nik', $id);
		return $this->db->delete('_teacher');
	}

}

/* End of file Teachers_model.php */
/* Location: ./application/modules/webadmin/models/Teachers_model.php */