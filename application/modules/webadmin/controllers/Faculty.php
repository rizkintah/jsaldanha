<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('faculty_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	function index()
	{
		$data['title'] = 'Management Faculty';
		$this->load->view('page_faculty', $data);
	}

	public function add_faculty()
	{
		$data['title'] = "Add Data Faculty";
		$data['program'] = $this->faculty_model->get_list_program();
		$this->load->view('page_add_faculty', $data);
	}

	function add_faculty_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$faculty_name = $this->input->post('faculty_name');
			$description  = $this->input->post('description');
			$address 	  = $this->input->post('address');
			$program_id	  = $this->input->post('program_id');

			$data_insert = array(
					'faculty_name'	=> $faculty_name,
					'faculty_desc'	=> $description,
					'faculty_addr'	=> $address,
					'program_id'	=> $program_id,
				);

			$insert = $this->faculty_model->insert_faculty_to_db($data_insert);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Faculty successful added'));
				redirect('webadmin/faculty');	
			}
		}else{
			redirect('webadmin/faculty');
		}
	}

	function get_list_faculty_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$stext = $this->input->get('search');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');

			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY faculty_name ASC";
			}

			$limit2 = $offset + $limit;
			$list_menu_category = $this->faculty_model->get_list_faculty_ajax($order,$limit2,$offset,$stext);
			echo (json_encode($list_menu_category));
		}else{
		   exit('No direct script access allowed');
		}
	}

	/****************** EDIT FUNCTION ***********************/ 
	function edit_faculty($id)
	{
		$data['title'] = "Edit Data Faculty";
		$data['program'] = $this->faculty_model->get_list_program();
		$data['faculty'] = $this->faculty_model->get_data_faculty($id);
		
		$this->load->view('page_edit_faculty', $data);
	}
	
	function edit_faculty_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$faculty_name = $this->input->post('faculty_name');
			$description  = $this->input->post('description');
			$address 	  = $this->input->post('address');
			$id_data 	  = $this->input->post('id_data');

			$data_where = array('id' => $id_data);
			$data_update = array(
					'faculty_name'	=> $faculty_name,
					'faculty_desc'	=> $description,
					'faculty_addr'	=> $address,
				);

			$update = $this->faculty_model->update_faculty_to_db($data_update,$data_where);
			if ($update) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Faculty successful edited'));
				redirect('webadmin/faculty');	
			}
		}else{
			redirect('webadmin/faculty');
		}
	}
	
	/****************** DELETE FUNCTION ***********************/ 
	
	function delete_faculty($id)
	{
		$data_where = array('id' => $id);
		$delete = $this->faculty_model->delete_faculty_from_db($data_where);
		if ($delete) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Faculty successful deleted'));
			redirect('webadmin/faculty');	
		}
	}
}

/* End of file Faculty.php */
/* Location: ./application/modules/webadmin/controllers/Faculty.php */