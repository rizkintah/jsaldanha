<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classroom extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('classroom_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	public function index()
	{
		$data['title'] = 'Management Class';
		$this->load->view('page_classroom', $data);
	}

	function get_list_classroom_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY class_name ASC";
				}
			$list_students = $this->classroom_model->get_list_classroom_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}else{
			echo "sorry not allowed";
		}
	}

	function add_classroom()
	{
		$data['title'] = 'Add Data Class';
		$this->load->view('page_add_classroom', $data);
	}

	function add_classroom_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$class_name = $this->input->post('class_name');
			$class_desc = $this->input->post('class_desc');

			$data = array(
					'class_name'			=> $class_name,
					'class_desc'			=> $class_desc,
				);

			$insert = $this->classroom_model->insert_to_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Data successful added'));
				redirect('webadmin/classroom');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function edit_classroom($id)
	{
		$data['title'] = 'Edit Data Class';
		$data['detail'] = $this->classroom_model->get_detail($id)->row_array();
		$this->load->view('page_edit_classroom', $data);
	}

	function edit_classroom_process($id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$class_name = $this->input->post('class_name');
			$class_desc = $this->input->post('class_desc');

			$data = array(
					'class_name'			=> $class_name,
					'class_desc'			=> $class_desc,
				);

			$insert = $this->classroom_model->update_to_db($data,array('id'=>$id));
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Data successful edited'));
				redirect('webadmin/classroom');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function delete_classroom($id)
	{
		$del = $this->classroom_model->delete_classroom($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Data successful deleted'));
			redirect('webadmin/classroom');
		}
	}

}

/* End of file Classroom.php */
/* Location: ./application/modules/webadmin/controllers/Classroom.php */