<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webadmin extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// Modules::run('login/check_isvalidated');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	public function index()
	{

		// $data['title']  = "Login Administrator";
		// $this->load->view('page_login', $data);	
		$this->dashboard();
	}

	public function login()
	{
		$data['title']  = "Login Administrator";
		$this->load->view('page_login', $data);	
	}


	public function dashboard() {
		$data['title']  = "Administrator Area";
		$this->load->view('page_home', $data);		
	}

	public function management_faculty() {
		$data['title'] = 'Management Faculty';
		$this->load->view('page_faculty', $data);
	}

}

/* End of file Webadmin.php */
/* Location: ./application/modules/webadmin/controllers/Webadmin.php */