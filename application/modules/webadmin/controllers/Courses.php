<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('courses_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	public function manage_courses()
	{
		$data['title'] = 'Management Courses';
		$this->load->view('page_courses', $data);
	}

	function get_list_courses_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY department_name ASC";
				}
			$list_students = $this->courses_model->get_list_courses_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}else{
			echo "sorry not allowed";
		}
	}

	function add_courses()
	{
		$data['title'] = 'Add Data Course';
		$data['gradeList'] = $this->courses_model->get_list_grade();
		$this->load->view('page_add_courses', $data);	
	}

	function add_course_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$courses_name = $this->input->post('courses_name');
			$department_id = $this->input->post('department_id');
			$grade_id = $this->input->post('grade_id');
			$courses_sks = $this->input->post('courses_sks');

			$data = array(
					'courses_name'			=> $courses_name,
					'department_id'			=> $department_id,
					'grade_id'				=> $grade_id,
					'courses_sks'			=> $courses_sks,
				);

			$insert = $this->courses_model->insert_to_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Course successful added'));
				redirect('webadmin/courses/manage_courses');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function edit_course($id)
	{
		$data['title'] = 'Edit Data Course';
		$data['gradeList'] = $this->courses_model->get_list_grade();
		$data['detail'] = $this->courses_model->get_detail($id)->row_array();
		$this->load->view('page_edit_courses', $data);	
	}

	function edit_course_process($id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$courses_name = $this->input->post('courses_name');
			$department_id = $this->input->post('department_id');
			$grade_id = $this->input->post('grade_id');
			$courses_sks = $this->input->post('courses_sks');

			$data = array(
					'courses_name'			=> $courses_name,
					'department_id'			=> $department_id,
					'grade_id'				=> $grade_id,
					'courses_sks'			=> $courses_sks,
				);

			$insert = $this->courses_model->update_to_db($data,array('id'=>$id));
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Course successful edited'));
				redirect('webadmin/courses/manage_courses');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function delete_course($id)
	{
		$del = $this->courses_model->delete_course($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Course successful deleted'));
			redirect('webadmin/courses/manage_courses');
		}
	}

# ****************** TEACHER COURSE METHOD ************************* 
	function teacher_courses()
	{
		$data['title'] = 'Management Teacher Courses';
		$this->load->view('page_teacher_courses', $data);
	}

	function get_list_teacher_courses_ajax()
	{
		if ($this->input->is_ajax_request()) {	
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY department_name ASC";
				}
			$list_students = $this->courses_model->get_list_teacher_courses_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}else{
			echo "sorry not allowed";
		}
	}

	function get_list_teacher_json()
	{
		if ($this->input->is_ajax_request()) {
			$term = $this->input->get('q');
			$json_list = $this->courses_model->get_list_teacher_json($term);
			echo json_encode($json_list->result_array());  
		}
	}

	function get_list_courses_json()
	{
		if ($this->input->is_ajax_request()) {
			$term = $this->input->get('q');
			$json_list = $this->courses_model->get_list_courses_json($term);
			echo json_encode($json_list->result_array());  
		}
	}

	function add_teacher_courses()
	{
		$data['title'] = 'Add Data Teacher Course';
		$this->load->view('page_add_teacher_courses', $data);	
	}

	function add_teacher_course_process($value='')
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$teacher_id = $this->input->post('teacher_id');
			$courses_id = $this->input->post('courses_id');
			$class_id = $this->input->post('class_id');

			$data = array(
					'teacher_id'			=> $teacher_id,
					'courses_id'			=> $courses_id,
					'class_id'				=> $class_id,
				);

			$insert = $this->courses_model->insert_to_db_teacher_course($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Teacher Course & Class successful added'));
				redirect('webadmin/courses/teacher_courses');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function edit_teacher_course($id)
	{
		$data['title'] = 'Edit Data Teacher Course';
		$data['detail'] = $this->courses_model->get_detail_teacher_course($id)->row_array();
		$this->load->view('page_edit_teacher_courses', $data);	
	}

	function edit_teacher_course_process($id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$teacher_id = $this->input->post('teacher_id');
			$courses_id = $this->input->post('courses_id');
			$class_id = $this->input->post('class_id');

			$data = array(
					'teacher_id'			=> $teacher_id,
					'courses_id'			=> $courses_id,
					'class_id'				=> $class_id,
				);

			$insert = $this->courses_model->update_to_teacher_course($data,array('id'=>$id));
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Teacher Course & Class successful edited'));
				redirect('webadmin/courses/teacher_courses');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function delete_teacher_course($id)
	{
		$del = $this->courses_model->delete_teacher_course($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Teacher Course & Class successful deleted'));
			redirect('webadmin/courses/teacher_courses');
		}
	}

}

/* End of file Courses.php */
/* Location: ./application/modules/webadmin/controllers/Courses.php */