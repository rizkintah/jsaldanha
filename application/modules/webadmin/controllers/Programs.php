<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Programs extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('programs_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	public function index()
	{
		$data['title'] = 'Management Programs';
		$this->load->view('page_programs', $data);
	}

	function get_list_programs_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$stext = $this->input->get('search');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');

			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY sortby ASC";
			}

			$limit2 = $offset + $limit;
			$data = $this->programs_model->get_list_programs_ajax($order,$limit2,$offset,$stext);
			echo (json_encode($data));
		}else{
		   exit('No direct script access allowed');
		}
	}


	public function add_program()
	{
		$data['title'] = "Add Data Program";
		$data['Jmlprogram'] = count($this->programs_model->get_list_program());
		$this->load->view('page_add_program', $data);
	}

	function add_program_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$program_name = $this->input->post('program_name');
			$description  = $this->input->post('description');
			$sorting	  = $this->input->post('sort');

			$data_insert = array(
					'program_name'	=> $program_name,
					'program_desc'	=> $description,
					'program_url'		=> url_title($program_name),
					'sortby'	=> $sorting
				);

			$insert = $this->programs_model->insert_programs_to_db($data_insert);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Program successful added'));
				redirect('webadmin/programs');	
			}
		}else{
			redirect('webadmin/programs');
		}
	}


	function edit_programs($id)
	{
		$data['title'] = "Edit Data Program";
		$data['detail'] = $this->programs_model->get_data_program($id);
		
		$this->load->view('page_edit_program', $data);
	}
	
	function edit_faculty_process($id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$program_name = $this->input->post('program_name');
			$description  = $this->input->post('description');
			$sorting	  = $this->input->post('sort');

			$data_update = array(
					'program_name'	=> $program_name,
					'program_desc'	=> $description,
					'program_url'		=> url_title($program_name),
					'sortby'				=> $sorting
				);

			$update = $this->programs_model->update_programs_to_db($data_update,array('id' => $id));
			if ($update) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Program successful edited'));
				redirect('webadmin/programs');	
			}
		}else{
			redirect('webadmin/programs');
		}
	}


	function delete_programs($id)
	{
		$data_where = array('id' => $id);
		$delete = $this->programs_model->delete_programs_from_db($data_where);
		if ($delete) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Program successful deleted'));
			redirect('webadmin/programs');	
		}
	}

}

/* End of file Programs.php */
/* Location: ./application/modules/webadmin/controllers/Programs.php */