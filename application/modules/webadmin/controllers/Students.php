<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('students_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	public function index()
	{
		$data['title'] = 'Management Students';
		$this->load->view('page_students', $data);
	}

	function get_list_student_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY student_name ASC";
				}
			$list_students = $this->students_model->get_list_students_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}
	}

	/***************** ADD FUNCTION ****************************/

	function add_students()
	{
		$data['title'] = 'Add Data Students';
		$this->load->view('page_add_students', $data);
	}

	function add_students_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$student_id = $this->input->post('student_id');
			$student_name = $this->input->post('student_name');
			$department_id = $this->input->post('department_id');
			$class_id = $this->input->post('class_id');
			$student_phone = $this->input->post('student_phone');
			$student_mobile = $this->input->post('student_mobile');
			$student_email = $this->input->post('student_email');
			$student_addr = $this->input->post('student_addr');
			$student_birth_place = $this->input->post('student_birth_place');
			$student_birthday = $this->input->post('student_birthday');
			$student_status = $this->input->post('student_status');
			$academic_year = $this->input->post('academic_year');
			$password = $this->encrypt->encode($this->input->post('password'));
			$program_id = $this->input->post('program_id');
			$faculty_id = $this->input->post('faculty_id');
			$gender = $this->input->post('gender');
			$deptId = $department_id ;
			if (empty($department_id)) {
				$deptId=$faculty_id;
			}

			$newName = strtolower($student_id.'_'.$_FILES['student_photo']['name']);
			$uploaded_file = $this->uploaded_files('student_photo','./_media/_students/',$newName);
			$data = array(
					'student_id' 			=> $student_id,
					'student_name' 			=> $student_name,
					'department_id' 		=> $deptId,
					'class_id' 				=> $class_id,
					'student_phone' 		=> $student_phone,
					'student_mobile' 		=> $student_mobile,
					'student_email' 		=> $student_email,
					'student_addr' 			=> $student_addr,
					'student_birth_place'	=> $student_birth_place,
					'student_birthday'		=> $student_birthday,
					'student_photo'			=> $uploaded_file['file_name'],
					'student_status'		=> $student_status,
					'academic_year'			=> $academic_year,
					'password'				=> $password,
					'gender'				=> $gender,
				);
			$insert = $this->students_model->insert_to_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Student successful added'));
				redirect('webadmin/students');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	/************* EDIT FUNCTION ****************/
	function edit_student($student_id)
	{
		$data['detail'] = $this->students_model->get_detail($student_id)->row_array();
		$data['title'] = 'Edit Data Students';
		$this->load->view('page_edit_students', $data);
	} 

	function edit_students_process($student_id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$student_name = $this->input->post('student_name');
			$program_id = $this->input->post('program_id');
			$faculty_id = $this->input->post('faculty_id');
			$department_id = $this->input->post('department_id');
			$class_id = $this->input->post('class_id');
			$student_phone = $this->input->post('student_phone');
			$student_mobile = $this->input->post('student_mobile');
			$student_email = $this->input->post('student_email');
			$student_addr = $this->input->post('student_addr');
			$student_birth_place = $this->input->post('student_birth_place');
			$student_birthday = $this->input->post('student_birthday');
			$student_status = $this->input->post('student_status');
			$academic_year = $this->input->post('academic_year');
			$password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
			$gender = $this->input->post('gender');
			$deptId = $department_id ;
			if (empty($department_id)) {
				$deptId=$faculty_id;
			}

			$data = array(
					'student_id' 			=> $this->input->post('old_id_student'),
					'student_name' 			=> $student_name,
					'department_id' 		=> $deptId,
					'class_id' 				=> $class_id,
					'student_phone' 		=> $student_phone,
					'student_mobile' 		=> $student_mobile,
					'student_email' 		=> $student_email,
					'student_addr' 			=> $student_addr,
					'student_birth_place'	=> $student_birth_place,
					'student_birthday'		=> $student_birthday,
					'student_status'		=> $student_status,
					'academic_year'			=> $academic_year,
					'gender'				=> $gender,
				);

			# update pwd
			if (!empty($this->input->post('password'))) $data['password'] = $password;

			if (!empty($_FILES['student_photo']['name'])) {
				$newName = strtolower($student_id.'_'.$_FILES['student_photo']['name']);
				$uploaded_file = $this->uploaded_files('student_photo','./_media/_students/',$newName);
				$data['student_photo'] =  $uploaded_file['file_name'];
			}
			
			$insert = $this->students_model->update_to_db($data,array('student_id'=>$student_id));
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Student successful edited'));
				redirect('webadmin/students');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function delete_student($student_id)
	{
		$del = $this->students_model->delete($student_id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Student successful deleted'));
			redirect('webadmin/students');
		}
	}

	function get_list_fac_json()
	{
		if ($this->input->is_ajax_request()) {
			$term = $this->input->get('q');
			$json_list = $this->students_model->get_list_fac_json($term);
			echo json_encode($json_list->result_array());  
		}
	}

	function get_list_dept_json()
	{
		if ($this->input->is_ajax_request()) {
			$term = $this->input->get('q');
			$json_list = $this->students_model->get_list_dept_json($term);
			echo json_encode($json_list->result_array());  
		}
	}

	function get_list_class_json()
	{
		if ($this->input->is_ajax_request()) {
			$term = $this->input->get('q');
			$json_list = $this->students_model->get_list_class_json($term);
			echo json_encode($json_list->result_array());  
		}
	}


	/****************** ONLINE REG **************************/
		function online_registration()
		{
			$data['title'] = 'Management Online Registration';
			$this->load->view('page_onreg', $data);
		}


		function get_list_student_onreg_ajax()
		{
			// if ($this->input->is_ajax_request()) {
				$sort = $this->input->get('sort');
				$order = $this->input->get('order');
				$limit = $this->input->get('limit');
				$offset = $this->input->get('offset');
				$filter = json_decode($this->input->get('filter'),TRUE);
				$limit2 = $offset + $limit;
				if ((!empty($sort)) && (!empty($order))) {
						$order = "ORDER BY $sort $order";
					}else{
						$order = "ORDER BY student_id ASC";
					}
				$list_students = $this->students_model->get_list_students_onreg_ajax($order,$limit2,$offset,$filter);
				echo (json_encode($list_students));
			// }
		}

		function view_detail_onreg($student_id)
		{
			$data['title'] = 'View Detail Online Registration';
			$data['detail'] = $this->students_model->view_detail_onreg($student_id)->row_array();
			$this->load->view('page_view_onreg', $data);
		}
	/****************** END ONLINE REG **************************/ 

	function uploaded_files($filename,$upload_path,$newName){
		$this->load->library('upload');
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = '*';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $newName;
		// $config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->upload->initialize($config);

		if (!$this->upload->do_upload($filename))
		{
		    // case - failure
		    $upload_error = array('error' => $this->upload->display_errors());
		}
		else
		{
		    // case - success
		    $upload_data = $this->upload->data();
		    return $upload_data;
		}
    }

}

/* End of file Students.php */
/* Location: ./application/modules/webadmin/controllers/Students.php */