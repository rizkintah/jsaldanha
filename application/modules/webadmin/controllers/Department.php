<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('department_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	function index()
	{
		$data['title'] = 'Management Department';
		$this->load->view('page_department', $data);
	}

	public function add_department()
	{
		$data['title'] = "Add Data Department";
		$data['list_faculty'] = $this->department_model->get_list_faculty();
		$this->load->view('page_add_department', $data);
	}

	function add_department_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$faculty_id = $this->input->post('faculty_id');
			$department_name  = $this->input->post('department_name');
			$description 	  = $this->input->post('description');
			$status 	  = 'Active';

			$data_insert = array(
					'faculty_id'	=> $faculty_id,
					'department_name'	=> $department_name,
					'department_desc'	=> $description,
					'department_stat'	=> $status
				);
			
			$insert = $this->department_model->insert_department_to_db($data_insert);
			if ($insert) {
				redirect('webadmin/department');	
			}
		}else{
			redirect('webadmin/department');
		}
	}

	function get_list_department_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$stext = $this->input->get('search');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');

			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY department_name ASC";
			}

			$limit2 = $offset + $limit;
			$list_menu_category = $this->department_model->get_list_department_ajax($order,$limit2,$offset,$stext);
			// print_r($list_menu_category);
			// exit;
			echo (json_encode($list_menu_category));
		}else{
		   exit('No direct script access allowed');
		}
	}

	/****************** EDIT FUNCTION ***********************/ 
	function edit_department($id)
	{
		$data['title'] = "Edit Data Department";
		$data['department'] = $this->department_model->get_data_department($id)->row_array();
		$data['list_faculty'] = $this->department_model->get_list_faculty();
		$this->load->view('page_edit_department', $data);
	}
	
	function edit_department_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$faculty_id = $this->input->post('faculty_id');
			$department_name  = $this->input->post('department_name');
			$description 	  = $this->input->post('description');
			$status 	  = 'Active';
			$id_data 	  = $this->input->post('id_data');

			$data_where = array('id' => $id_data);
			$data_update = array(
					'faculty_id'	=> $faculty_id,
					'department_name'	=> $department_name,
					'department_desc'	=> $description,
					'department_stat'	=> $status
				);

			$update = $this->department_model->update_department_to_db($data_update,$data_where);
			if ($update) {
				redirect('webadmin/department');	
			}
		}else{
			redirect('webadmin/department');
		}
	}
	
	/****************** DELETE FUNCTION ***********************/ 
	
	function delete_department($id)
	{
		$data_where = array('id' => $id);
		$delete = $this->department_model->delete_department_from_db($data_where);
		if ($delete) {
			redirect('webadmin/department');	
		}
	}
}

/* End of file Faculty.php */
/* Location: ./application/modules/webadmin/controllers/Faculty.php */