<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_pages extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
		$this->load->model('manage_pages_model');
		
	}
	
	function manage_slide() {
		$data['title'] = 'Pages Manage Slide';
		$this->load->view('page_manage_slide', $data);	
	}

	function get_list_slides_ajax() {
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY id ASC";
			}

			$limit2 = $offset + $limit;
			$list_galleries = $this->manage_pages_model->get_list_slides_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_galleries));
		}else{
		   exit('No direct script access allowed');
		}
	}

	function add_images_slide()
	{
		$data['title'] = 'Add Image';
		$this->load->view('page_add_images_slide', $data);
	}

	function add_images_slide_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$newName = date('Ymd').'_'.$_FILES['img_filename']['name'];
			$uploaded_file = $this->uploaded_files('img_filename','./_media/galleries/',$newName);
			$data = array(
					'img_file'	=> $uploaded_file['file_name']
				);

			$insert = $this->manage_pages_model->insert_slides_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','Image successful Added'));
				redirect('webadmin/manage_pages/manage_slide');
			}
		}else{
			redirect('webadmin/manage_pages/manage_slide');
		}
	}

	function delete_images_slide($id)
	{
		$del = $this->manage_pages_model->delete_slides($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','Image successful deleted'));
			redirect('webadmin/manage_pages/manage_slide');	
		}
	}
	
	function about_jsu()
	{
		$data['title'] = 'Pages About';
		$data['detail'] = $this->manage_pages_model->get_pages_detail('about_jsu')->row_array();
		$this->load->view('page_about', $data);	
	}

	function update_about_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$data = array('content_txt' => $this->input->post('about'));
			$update = $this->manage_pages_model->update_pages_db($data,'about_jsu');
			if ($update) {
				redirect('webadmin/manage_pages/about_jsu');
			}
		}else{
			redirect('webadmin/manage_pages/about_jsu');
		}
	}

	function staffs()
	{
		$data['title'] = 'Pages Staffs';
		$data['detail'] = $this->manage_pages_model->get_pages_detail('staffs')->row_array();
		$this->load->view('page_staff', $data);	
	}

	function update_staffs_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$data = array('content_txt' => $this->input->post('staffs'));
			$update = $this->manage_pages_model->update_pages_db($data,'staffs');
			if ($update) {
				redirect('webadmin/manage_pages/staffs');
			}
		}else{
			redirect('webadmin/manage_pages/staffs');
		}
	}

	function news_event()
	{
		$data['title'] = 'Pages News and Events';
		$this->load->view('page_news_event', $data);	
	}

	function add_news_event()
	{
		$data['title'] = "Add News and Event";
		$this->load->view('page_add_news_event', $data);
	}

	function add_news_event_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$title = $this->input->post('title');
			$event_date = $this->input->post('event_date');
			$description = $this->input->post('description');
			$url = seo_url($title);

			$data = array(
					'url_en' 	 => $url,
					'title_en' 	 => $title,
					'date_en' 	 => date('Y-m-d',strtotime($event_date)),
					'content_en' => $description,
				);
			$insert = $this->manage_pages_model->insert_event_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','News and Event successful added'));
				redirect('webadmin/manage_pages/news_event');	
			}

		}else{
			redirect('webadmin/manage_pages/news_event');
		}
	}

	function get_list_news_event_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY date_en ASC";
			}

			$limit2 = $offset + $limit;
			$list_news_event = $this->manage_pages_model->get_list_news_event_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_news_event));
		}else{
		   exit('No direct script access allowed');
		}
	}

	function edit_news_event()
	{
		$id = $this->uri->segment(4);
		$data['title'] = "Edit News and Event";
		$data['detail'] = $this->manage_pages_model->get_detail_edit_news_event($id)->row_array();
		$this->load->view('page_edit_news_event', $data);
	}

	function edit_news_event_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$id=$this->uri->segment(4);
			$title = $this->input->post('title');
			$event_date = $this->input->post('event_date');
			$description = $this->input->post('description');
			$url = seo_url($title);

			$data = array(
					'url_en' 	 => $url,
					'title_en' 	 => $title,
					'date_en' 	 => date('Y-m-d',strtotime($event_date)),
					'content_en' => $description,
				);
			
			$update = $this->manage_pages_model->update_edit_news_event($id,$data);
			if ($update) {
				$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','News and Event successful updated'));
				redirect('webadmin/manage_pages/news_event');	
			}

		}else{
			redirect('webadmin/manage_pages/news_event');
		}
	}
	
	function delete_news_event()
	{
		$id=$this->uri->segment(4);
		$this->db->where('id', $id);
		$del = $this->db->delete('_news_event');
		if ($del) {
			$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','News and Event successful deleted'));
				redirect('webadmin/manage_pages/news_event');	
		}
	}

	function galleries()
	{
		$data['title'] = 'Pages Galleries';
		$this->load->view('page_galleries', $data);
	}

	function get_list_galleries_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY title ASC";
			}

			$limit2 = $offset + $limit;
			$list_galleries = $this->manage_pages_model->get_list_galleries_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_galleries));
		}else{
		   exit('No direct script access allowed');
		}
	}

	function add_images()
	{
		$data['title'] = 'Add Image';
		$this->load->view('page_add_images', $data);
	}

	function add_images_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$title = $this->input->post('title');
			$newName = date('Ymd').'_'.$_FILES['img_filename']['name'];
			$uploaded_file = $this->uploaded_files('img_filename','./_media/galleries/',$newName);
			$data = array(
					'title' 		=> $title,
					'img_filename'	=> $uploaded_file['file_name']
				);

			$insert = $this->manage_pages_model->insert_galleries_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','Image successful Added'));
				redirect('webadmin/manage_pages/galleries');
			}
		}else{
			redirect('webadmin/manage_pages/galleries');
		}
	}

	function delete_galleries($id)
	{
		$del = $this->manage_pages_model->delete_galleri($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','Image successful deleted'));
			redirect('webadmin/manage_pages/galleries');	
		}
	}

	function contact()
	{
		$data['title'] = 'Pages Contact';
		$data['detail'] = $this->manage_pages_model->get_pages_detail('contact')->row_array();
		$this->load->view('page_contact', $data);	
	}

	function update_contact_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$data = array('content_txt' => $this->input->post('contact'));
			$update = $this->manage_pages_model->update_pages_db($data,'contact');
			if ($update) {
				redirect('webadmin/manage_pages/contact');
			}
		}else{
			redirect('webadmin/manage_pages/contact');
		}
	}


	function showErrorPopOver($alert_type,$textLine){
    	$alert = "<div class='alert ".$alert_type." alert-dismissible fade in' role='alert'>
              <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
              </button>
              ".$textLine."
          </div>";

       return $alert;
  	}

  	/************ ADMISSIONS ***************/ 
  	function admissions()
	{
		$data['title'] = 'Pages Admissions';
		$this->load->view('page_admissions', $data);	
	}

	function add_admissions()
	{
		$data['title'] = "Add Admissions";
		$this->load->view('page_add_admissions', $data);
	}

	function get_list_admissions_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY date_en ASC";
			}

			$limit2 = $offset + $limit;
			$list_news_event = $this->manage_pages_model->get_list_admissions_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_news_event));
		}else{
		   exit('No direct script access allowed');
		}
	}

	function add_admission_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$title = $this->input->post('title');
			$event_date = $this->input->post('event_date');
			$description = $this->input->post('description');
			$url = seo_url($title);

			$data = array(
					'url_en' 	 => $url,
					'title_en' 	 => $title,
					'date_en' 	 => date('Y-m-d',strtotime($event_date)),
					'content_en' => $description,
				);
			$insert = $this->manage_pages_model->insert_admission_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','Admission successful added'));
				redirect('webadmin/manage_pages/admissions');	
			}

		}else{
			redirect('webadmin/manage_pages/admissions');
		}
	}

	function edit_admissions()
	{
		$id = $this->uri->segment(4);
		$data['title'] = "Edit Admission";
		$data['detail'] = $this->manage_pages_model->get_detail_edit_admissions($id)->row_array();
		$this->load->view('page_edit_admissions', $data);
	}

	function edit_admission_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$id=$this->uri->segment(4);
			$title = $this->input->post('title');
			$event_date = $this->input->post('event_date');
			$description = $this->input->post('description');
			$url = seo_url($title);

			$data = array(
					'url_en' 	 => $url,
					'title_en' 	 => $title,
					'date_en' 	 => date('Y-m-d',strtotime($event_date)),
					'content_en' => $description,
				);
			
			$update = $this->manage_pages_model->update_edit_admissions($id,$data);
			if ($update) {
				$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','Admission successful updated'));
				redirect('webadmin/manage_pages/admissions');	
			}

		}else{
			redirect('webadmin/manage_pages/admissions');
		}
	}

	function delete_admissions()
	{
		$id=$this->uri->segment(4);
		$this->db->where('id', $id);
		$del = $this->db->delete('_admissions');
		if ($del) {
			$this->session->set_flashdata('alertFlash',$this->showErrorPopOver('alert-success','Admissions successful deleted'));
				redirect('webadmin/manage_pages/admissions');	
		}
	}

	/******** END ADMISSIONS ***/ 

  	function uploaded_files($filename,$upload_path,$newName){
		$this->load->library('upload');
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = '*';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $newName;
		// $config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->upload->initialize($config);

		if (!$this->upload->do_upload($filename))
		{
		    // case - failure
		    $upload_error = array('error' => $this->upload->display_errors());
		}
		else
		{
		    // case - success
		    $upload_data = $this->upload->data();
		    return $upload_data;
		}
    }

}

/* End of file Manage_pages.php */
/* Location: ./application/modules/webadmin/controllers/Manage_pages.php */