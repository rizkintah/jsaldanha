<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('teachers_model');
		if (!$this->session->userdata('validated')) {
			redirect('/login/administrator');
			exit();
		}
	}

	function index()
	{
		$data['title'] = 'Management Lectures';
		$this->load->view('page_teachers', $data);		
	}

	function get_list_teachers_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			if ((!empty($sort)) && (!empty($order))) {
				$order = "ORDER BY $sort $order";
			}else{
				$order = "ORDER BY teacher_name ASC";
			}

			$limit2 = $offset + $limit;
			$list_teachers = $this->teachers_model->get_list_teachers_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_teachers));
		}else{
		   exit('No direct script access allowed');
		}
	}

	function add_teacher()
	{
		$data['title'] = 'Add Data Lecture';
		$this->load->view('page_add_teachers', $data);
	}

	function add_teacher_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$teacher_id		= $this->input->post('teacher_id');
			$teacher_name	= $this->input->post('teacher_name');
			$teacher_phone	= $this->input->post('teacher_phone');
			$teacher_mobile	= $this->input->post('teacher_mobile');
			$teacher_addr	= $this->input->post('teacher_addr');

			$data_insert = array(
					'teacher_nik'	=> $teacher_id,
					'teacher_name'	=> $teacher_name,
					'teacher_phone'	=> $teacher_phone,
					'teacher_mobile'=> $teacher_mobile,
					'teacher_addr'	=> $teacher_addr,
					'teacher_stat'	=> "Active"
				);

			$insert = $this->teachers_model->insert_teacher_db($data_insert);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Teacher successful added'));
				redirect('webadmin/teachers');	
			}
		}else{
			redirect('webadmin/teachers');
		}
	}


	function edit_teachers($id)
	{
		$data['title'] = 'Edit Data Lecture';
		$data['detail'] = $this->teachers_model->get_detail($id)->row_array();
		$this->load->view('page_edit_teachers', $data);	
	}

	function edit_teacher_process($id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$teacher_id		= $this->input->post('teacher_id');
			$teacher_name	= $this->input->post('teacher_name');
			$teacher_phone	= $this->input->post('teacher_phone');
			$teacher_mobile	= $this->input->post('teacher_mobile');
			$teacher_addr	= $this->input->post('teacher_addr');

			$data_insert = array(
					'teacher_name'	=> $teacher_name,
					'teacher_phone'	=> $teacher_phone,
					'teacher_mobile'=> $teacher_mobile,
					'teacher_addr'	=> $teacher_addr,
				);

			$insert = $this->teachers_model->update_teacher_db($data_insert,array('teacher_nik'=>$id));
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Teacher successful edited'));
				redirect('webadmin/teachers');	
			}
		}else{
			redirect('webadmin/teachers');
		}
	}

	function delete_teachers($id)
	{
		$del = $this->teachers_model->get_delete($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Teacher successful deleted'));
			redirect('webadmin/teachers');	
		}
	}


}

/* End of file Teachers.php */
/* Location: ./application/modules/webadmin/controllers/Teachers.php */