<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('facilities_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	function manage_main_facilities()
	{
		$data['title'] = 'Management Main Category Facilities';
		$this->load->view('page_main_facility', $data);	
	}

	function get_list_main_cat_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY fac_name ASC";
				}
			$list_students = $this->facilities_model->get_list_main_fac_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}else{
			echo "sorry not allowed";
		}
	}

	function add_main_fac()
	{
		$data['title'] = 'Add Main Category Facilities';
		$this->load->view('page_add_main_facility', $data);	
	}

	function add_main_cat_fac_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$fac_name = $this->input->post('fac_name');

			$data = array(
					'fac_name'			=> $fac_name,
				);

			$insert = $this->facilities_model->insert_to_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Main category facility successful added'));
				redirect('webadmin/facilities/manage_main_facilities');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function edit_main_cat($id)
	{
		$data['title'] = 'Edit Main Category Facilities';
		$data['detail'] = $this->facilities_model->get_facilities_detail($id)->row_array();
		$this->load->view('page_edit_main_facility', $data);	
	}

	function edit_main_cat_fac_process($id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$fac_name = $this->input->post('fac_name');

			$data = array(
					'fac_name'			=> $fac_name,
				);

			$insert = $this->facilities_model->update_to_db($data,array('id'=>$id));
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Main category facility successful edited'));
				redirect('webadmin/facilities/manage_main_facilities');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function delete_fac($id)
	{
		$del = $this->facilities_model->delete_fac($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Main category facility successful deleted'));
			redirect('webadmin/facilities/manage_main_facilities');
		}
	}


	// sub fac
	function manage_sub_facilities()
	{
		$data['title'] = 'Management Sub Category Facilities';
		$this->load->view('page_sub_facility', $data);	
	}

	function get_list_sub_fac_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY fac_name ASC";
				}
			$list_students = $this->facilities_model->get_list_sub_fac_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}else{
			echo "sorry not allowed";
		}
	}

	function add_sub_fac()
	{
		$data['title'] = 'Add Sub Category Facilities';
		$data['listFac'] = $this->facilities_model->get_list_main_cat();
		$this->load->view('page_add_sub_facility', $data);	
	}

	function add_sub_cat_fac_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$fac_name = $this->input->post('fac_name');
			$parent_id = $this->input->post('parent_id');
			$fac_desc = $this->input->post('fac_desc');

			$data = array(
					'fac_name'			=> $fac_name,
					'parent_id'			=> $parent_id,
					'fac_desc'			=> $fac_desc,
				);

			$insert = $this->facilities_model->insert_to_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Sub category facility successful added'));
				redirect('webadmin/facilities/manage_sub_facilities');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function edit_sub_cat($id)
	{
		$data['title'] = 'Edit Sub Category Facilities';
		$data['detail'] = $this->facilities_model->get_facilities_detail($id)->row_array();
		$data['listFac'] = $this->facilities_model->get_list_main_cat();
		$this->load->view('page_edit_sub_facility', $data);	
	}

	function edit_sub_cat_fac_process($id)
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$fac_name = $this->input->post('fac_name');
			$parent_id = $this->input->post('parent_id');
			$fac_desc = $this->input->post('fac_desc');

			$data = array(
					'fac_name'			=> $fac_name,
					'parent_id'			=> $parent_id,
					'fac_desc'			=> $fac_desc,
				);

			$insert = $this->facilities_model->update_to_db($data,array('id'=>$id));
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Sub category facility successful edited'));
				redirect('webadmin/facilities/manage_sub_facilities');
			}
		}else{
			echo "Sorry no allowed access";
		}
	}

	function delete_sub_fac($id)
	{
		$del = $this->facilities_model->delete_fac($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Sub category facility successful deleted'));
			redirect('webadmin/facilities/manage_sub_facilities');
		}
	}

}

/* End of file Facilities.php */
/* Location: ./application/modules/webadmin/controllers/Facilities.php */