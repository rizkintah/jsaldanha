<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('library_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	function index()
	{
		$data['title'] = 'Management Library';
		$this->load->view('page_library', $data);
	}

	function get_list_lib_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY department_name ASC";
				}
			$list_students = $this->library_model->get_list_lib_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}else{
			echo "sorry not allowed";
		}
	}

	function add_library()
	{
		$data['title'] = 'Add Data Library';
		$this->load->view('page_add_library', $data);
	}

	function add_lib_process()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$lib_title = $this->input->post('lib_title');
			$department_id = $this->input->post('department_id');
			$lib_desc = $this->input->post('lib_desc');

			$newName = $lib_title.'_'.$_FILES['lib_file']['name'];
			$uploaded_file = Modules::run('themes/uploaded_files', 'lib_file','./_media/_lib/',$newName);
			$data = array(
					'lib_title' 	=> $lib_title,
					'department_id' => $department_id,
					'lib_desc' 		=> $lib_desc,
					'lib_file'		=> $uploaded_file['file_name']
				);

			$insert = $this->library_model->insert_lib_to_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Data successful added'));
				redirect('webadmin/library');
			}
		}else{
			redirect('webadmin/library');
		}
	}

	function edit_library($id)
	{
		$data['title'] = 'Edita Data Library';
		$data['detail'] = $this->library_model->get_detail($id)->row_array();
		$this->load->view('page_edit_library', $data);
	}

	function edit_lib_process($id)
	{

		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$lib_title = $this->input->post('lib_title');
			$department_id = $this->input->post('department_id');
			$lib_desc = $this->input->post('lib_desc');
			$file_old = $this->input->post('file_old');
			$data = array(
					'lib_title' 	=> $lib_title,
					'department_id' => $department_id,
					'lib_desc' 		=> $lib_desc,
				);

			if (!empty($_FILES['lib_file']['name'])) {
				@unlink('./_media/_lib/'.$file_old);
				$newName = $lib_title.'_'.$_FILES['lib_file']['name'];
				$uploaded_file = Modules::run('themes/uploaded_files', 'lib_file','./_media/_lib/',$newName);
				$data['lib_file'] =  $uploaded_file['file_name'];
			}

			$update = $this->library_model->update_lib_db($data,array('id'=>$id));
			if ($update) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Data successful edited'));
				redirect('webadmin/library');
			}
		}else{
			redirect('webadmin/library');
		}
	}

	function delete_library($id)
	{
		$del = $this->library_model->delete_lib($id);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Data successful deleted'));
			redirect('webadmin/library');
		}
	}


}

/* End of file Library.php */
/* Location: ./application/modules/webadmin/controllers/Library.php */