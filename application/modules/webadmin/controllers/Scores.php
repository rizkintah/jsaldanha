<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scores extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('scores_model');
		if (!$this->session->userdata('validated')) {
			// Modules::run('webadmin/login');
			redirect('/login/administrator');
			exit();
		}
	}

	public function index()
	{
		$data['title'] = 'Management Student Score';
		$this->load->view('page_score_dept', $data);
	}

	function get_list_student_ajax()
	{
		if ($this->input->is_ajax_request()) {
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');
			$limit = $this->input->get('limit');
			$offset = $this->input->get('offset');
			$filter = json_decode($this->input->get('filter'),TRUE);
			$limit2 = $offset + $limit;
			if ((!empty($sort)) && (!empty($order))) {
					$order = "ORDER BY $sort $order";
				}else{
					$order = "ORDER BY student_name ASC";
				}
			$list_students = $this->scores_model->get_list_students_ajax($order,$limit2,$offset,$filter);
			echo (json_encode($list_students));
		}
	}

	function upload_scores()
	{
		if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
			$student_id = $this->input->post('student_id');
			$grade = $this->input->post('grade');
			$newName = strtolower($student_id.'_grade_'.$grade.'_'.$_FILES['score_file']['name']);
			$uploaded_file = $this->uploaded_files('score_file','./_media/_var/',$newName);

			$data = array(
					'grade_id'		=> $grade,
					'student_id'	=> $student_id,
					'file_score'	=> $uploaded_file['file_name']
				);
			$insert = $this->scores_model->insert_to_db($data);
			if ($insert) {
				$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-success','Score Student ID - '.$student_id.' successful upload'));
				redirect('webadmin/scores');
			}
		}
	}

	function get_list_score()
	{
		$student_id = $this->input->get('student_id');
		// echo $student_id;
		$list = $this->scores_model->get_list_score($student_id);
		$listHtml = "";
		$i=1;
		if ($list->num_rows() > 0) {
			foreach ($list->result() as $vals) {
				$listHtml = "<tr>
								<td>".$i."</td>
								<td> Grade ".$vals->grade_id."</td>
								<td>".$vals->file_score."</td>
								<td><a href='".base_url('webadmin/scores/delete_score/'.$vals->id.'/'.$vals->student_id)."' title='Delete' class='btn btn-xs btn-danger' onclick='return confirm(\"Are you sure want to delete this data?\")'>Delete</a></td>
							</tr>";
				$i++;
			}
		}else{
			$listHtml = "<tr><td colspan='4' align='center'>No Data Available</td></tr>";	
		}

		echo $listHtml;
		
	}

	function delete_score($id)
	{
		$del = $this->scores_model->delete($id);
		$student_id = $this->uri->segment(5);
		if ($del) {
			$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-info','Score Student ID - '.$student_id.' successful Delete'));
			redirect('webadmin/scores');
		}
	}

	function uploaded_files($filename,$upload_path,$newName){
		$this->load->library('upload');
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = '*';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $newName;
		// $config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->upload->initialize($config);

		if (!$this->upload->do_upload($filename))
		{
		    // case - failure
		    $upload_error = array('error' => $this->upload->display_errors());
		}
		else
		{
		    // case - success
		    $upload_data = $this->upload->data();
		    return $upload_data;
		}
    }


}

/* End of file scores.php */
/* Location: ./application/modules/webadmin/controllers/scores.php */