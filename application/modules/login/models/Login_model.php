<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	function oauth_user_admin($username, $password)
	{
		$this->db->select('*');
		$this->db->from('_admin');
		$this->db->where('username', $username);
		$this->db->where('status', "ACTIVE");
		$user = $this->db->get()->row_array();
		if ($this->verify_password_hash($password, $user['password'])) {
			$dataSession = array(
				'id_sess' 		=> $user['id'],
				'un_sess' 		=> $user['username'],
				'nama_sess' 	=> $user['fullname'],
				'tipe_sess' 	=> "ADMIN",
				'validated'		=> true
			);
			$this->session->set_userdata($dataSession);
		}
		return true;
		// return $this->verify_password_hash($password, $hash);
	}

	function oauth_user_student($username, $password)
	{
		$this->db->select('*');
		$this->db->from('_student');
		$this->db->where('student_id', $username);
		$this->db->where('student_status', "ACTIVE");
		$user = $this->db->get()->row_array();
		if ($this->verify_password_hash($password, $user['password'])) {
			$dataSession = array(
				'ses_student_id'		=> $user['student_id'],
				'oauth_valid'			=> true,
				'ses_dept_id'			=> $user['department_id'],
				'ses_class_id'			=> $user['class_id'],
				'sess_name'				=> $user['student_name'],
			);
			$this->session->set_userdata($dataSession);
			return true;
		}
		return false;
	}

	private function hash_password($password)
	{
		return password_hash($password, PASSWORD_BCRYPT);
	}

	private function verify_password_hash($password, $hash)
	{
		return password_verify($password, $hash);
	}	

}

/* End of file Login_model.php */
/* Location: ./application/modules/login/models/Login_model.php */