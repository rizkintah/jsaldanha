<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$title?></title>
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link href="<?=base_url()?>_assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/bootflat.min.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/css/style.css" rel="stylesheet" media="screen">
    <link href="<?=base_url()?>_assets/plugins/nprogress/nprogress.css" rel="stylesheet" media="screen">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header id="header">
        <div class="logo">
            <div class="container">
                <img src="<?=base_url()?>_assets/img/header.png" alt="jsu">
            </div>
        </div>   

        <nav class="navbar navbar-default navbar-jsu" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".menu_responsive">
                        <span><i class="glyphicon glyphicon-align-justify"></i> Menu</span>
                    </button>
                    <a class="navbar-brand hidden-md hidden-lg" href="<?=base_url()?>">JSU</a>
                </div>
        
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse menu_responsive">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                               Manage Pages <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">About JSU</a></li>
                                <li><a href="#">Facilities</a></li>
                                <li><a href="#">Staffs</a></li>
                                <li><a href="#">Schools</a></li>
                                <li><a href="#">News &amp; Events</a></li>
                                <li><a href="#">Galleries</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Faculty <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?=base_url()?>webadmin/management_faculty">Manage Faculty</a></li>
                                <li><a href="#">Manage Department</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Teachers</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Students <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Manage Students</a></li>
                                <li><a href="#">Manage Online Registration</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Courses</a></li>
                        <li><a href="#">Classroom</a></li>
                        <li><a href="#">Library</a></li>
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="glyphicon glyphicon-user"></i> Hi, <?=$this->session->userdata('nama_sess')?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Setting</a></li>
                                <li><a href="<?=base_url()?>login/logout_proccess">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="divider"></div>
                    <ul class="nav navbar-nav hidden-lg  hidden-md nav-sosial">
                        <li>
                            <a href="#"><img src="<?=base_url()?>_assets/img/fb-60.png" alt="Facebook"> Facebook</a>
                        </li>
                        <li>
                            <a href="#"><img src="<?=base_url()?>_assets/img/twitter-60.png" alt="Twitter"> Twitter</a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>

    </header><!-- /header -->

    <div class="container">
        