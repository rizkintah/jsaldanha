
	</div>


	<footer class="footer">
      <div class="container">
   		<h5 class="text-muted">"Local University, Global Responsibility"</h5>
        <ul class="nav nav-pills nav-footer hidden-xs hidden-sm">
            <li>
                <a href="#" target="_blank"><img src="<?=base_url()?>_assets/img/fb-60.png" alt="Facebook"></a>
            </li>
            <li>
                <a href="#" target="_blank"><img src="<?=base_url()?>_assets/img/twitter-60.png" alt="Twitter"></a>
            </li>
        </ul>
      </div>
    </footer>

	<script src="<?=base_url()?>_assets/js/jquery-1.12.3.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/modernizr-custom.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>_assets/plugins/nprogress/nprogress.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		var direct = "<?=base_url()?>";
		$(window).scroll(function() {
		  if ($(document).scrollTop() > 50) {
		    $('#header').addClass('to_fixed');
		  } else {
		    $('#header').removeClass('to_fixed');
		  }
		});
		
	</script>
</body>
</html>