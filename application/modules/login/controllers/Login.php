<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
* CREATED BY EDI RIZKINTA HARAHAP / 07-04-2016 / Bekasi
* rizkinta@gmail.com | 0822 1054 1720
* website: digitalsolutindo.com
*/ 

class Login extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('login_model');

	}

	public function index()
	{
		echo $uri = $this->uri->segment(2);
		
	}

	public function administrator()
	{
		$data['title']  = "Login Administrator";
		$this->load->view('page_login_admin', $data);	
	}

	function auth() {
		if($this->input->is_ajax_request())
    	{
    		// check csrf toke valid or not
			if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
				$tipeUrl = $this->input->post('url_id');
				$username = $this->input->post('un');
				$password = $this->input->post('pwd');
				if ($tipeUrl==strtolower("webadmin")) {
					$check_login = $this->login_model->oauth_user_admin($username, $password);
					if ($check_login) {
						echo 1;
					}else{
						echo 0;
					}
				}

			}else{
				redirect(base_url());
			}
		}else{
			redirect(base_url());
		}
	}

	function logout_proccess(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	function auth_student()
	{
		if($this->input->is_ajax_request())
    	{
    		// check csrf toke valid or not
			if (get_cookie('csrf_cookie_name')===$this->input->post('csrf_app_token')) {
				$username = $this->input->post('un');
				$password = $this->input->post('pwd');
				$check_login = $this->login_model->oauth_user_student($username, $password);
				if ($check_login) {
					echo 1;
				}else{
					echo 0;
					$this->session->set_flashdata('alertFlash', Modules::run('themes/showErrorPopOver', 'alert-danger','username or password wrong...'));

				}
			}else{
				redirect(base_url());
			}
		}else{
			redirect(base_url());
		}
	}

	function check_isvalidated_student(){
		if($this->session->userdata('validated')){
			redirect(base_url().'students/dashboard');
		}
	}

}

/* End of file Login.php */
/* Location: ./application/modules/login/controllers/Login.php */