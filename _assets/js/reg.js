$(function() {
	$.validator.setDefaults({
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');

	            var elem = $(element);
	            
	            if(elem.hasClass('s-select2')) {
	                var isMulti = (!!elem.attr('multiple')) ? 's' : '';
	                elem.siblings('.select2form').find('.select2-choice'+isMulti+'').addClass('has-error');            
	            } else {
	                elem.addClass('has-error');
	            }
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
		    errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } 
		        else {
		            error.insertAfter(element);
		        }
		    }
		});

	$("#frmReg").validate({
		rules: {
		    photo: {
		      required: true,
		      extension: "jpg|JPG|jpeg|png"
		    },
		    mobile_phone: {
		    	required:true,
		    	number:true
		    },
		    phone_number: {
		    	required:true,
		    	number:true
		    },
		    email: {
		    	required:true,
		    	email:true
		    }
	  	},
	    messages: {
	        photo: {
	            extension: "Please insert image extension (jpg,png)"
	        }
	    }
	});

    $("#inputProgram").change(function () {
    	$("#alertArea").html('');
    	var program = $(this).val();
    	$.ajax({
		    type: "GET",
		    url: encUrl+"get_program_faculty/"+program,
		    beforeSend: function () {
				NProgress.start();
			},
			success: function (data) {
				if (data == 0) {
					$("#alertArea").html('<div class="alert alert-danger" role="alert">Sorry program you choose is not yet available</div>');
					$(".frmCheck").attr('disabled',true);
				}else{
					$(".frmCheck").attr('disabled',false);
					if (program=="undergraduate") {
						$("#areaundergraduate").fadeIn();
						$("#areamagister").fadeOut();
						$("#inputundergraduate").html(data);	
					}else{
						$("#areaundergraduate").fadeOut();
						$("#areamagister").fadeIn();
						$("#inputmagister").html(data);
					}
					
				}
				NProgress.done();
			}
		});
    });

    $("#inputundergraduate").change(function () {
    	$("#alertArea").html('');
    	var program = $(this).val();
    	$.ajax({
		    type: "GET",
		    url: encUrl+"get_depart_faculty/"+program,
		    beforeSend: function () {
				NProgress.start();
			},
			success: function (data) {
				if (data == 0) {
					$("#alertArea").html('<div class="alert alert-danger" role="alert">Sorry faculty you choose is not yet available</div>');
					$(".frmCheck").attr('disabled',true);
					$(this).eq(1).attr('disabled',false);
					$(".frmCheck2").attr('disabled',false);
				}else{
					$(".frmCheck").attr('disabled',false);
					$("#inputdepartment").html(data);	
				}
				NProgress.done();
			}
		});
    });

    $('.datePick').datepicker({
	    todayBtn: "linked",
	    format:'dd/mm/yyyy',
	    autoclose: true,
	    todayHighlight: true
	});

});