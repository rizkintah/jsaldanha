$(document).ready(function () {
	$("#alertShow").css('display','none');
	$.validator.setDefaults({
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');

            var elem = $(element);
            
            if(elem.hasClass('s-select2')) {
                var isMulti = (!!elem.attr('multiple')) ? 's' : '';
                elem.siblings('.select2form').find('.select2-choice'+isMulti+'').addClass('has-error');            
            } else {
                elem.addClass('has-error');
            }
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    }
	});
	
	$("#frmLogin").validate();
});

$(document).on('submit','#frmLogin', function(e){
	var $un = $("#un").val();
	var $pwd = $("#pw").val();
	var $codeID = $("#code_id").val();
	var $url = $("#url_id").val();
	e.preventDefault();
	$.ajax({
		type: 'POST',
		url: direct+"login/auth",
		data: {'un': $un, 'pwd': $pwd,  'csrf_app_token': $codeID,'url_id':$url},
		beforeSend: function () {
			NProgress.start();
		},
		success: function (data) {
			console.log(data);
			if (data==1) {
				window.location.href = direct+"webadmin/dashboard";
			}else{
				window.location.href = direct+"webadmin";
			}
		},
		complete: function() {
			NProgress.done();
	   },
	   error: function (xhr, ajaxOptions, thrownError) {
          console.log(xhr.status);
          console.log(thrownError);
          console.log(xhr.responseText);
        }
	});
});

$(document).on('submit','#frmLoginStudent', function(e){
	var $un = $("#un").val();
	var $pwd = $("#pw").val();
	var $codeID = $("#code_id").val();
	e.preventDefault();
	$.ajax({
		type: 'POST',
		url: direct+"login/auth_student",
		data: {'un': $un, 'pwd': $pwd,  'csrf_app_token': $codeID},
		beforeSend: function () {
			NProgress.start();
		},
		success: function (data) {

			if (data==1) {
				window.location.href = direct+"students/dashboard";
			}else{
				setTimeout(function() {
				  window.location.href = direct+"students";
				}, 300);
				$("#alertShow").show();
			}
		},
		complete: function() {
			NProgress.done();
	   },
	   error: function (xhr, ajaxOptions, thrownError) {
          console.log(xhr.status);
          console.log(thrownError);
          console.log(xhr.responseText);
        }
	});
});
