const _appsJs = (function () {

	_accordionArrow = () => {
		$(".collapse.in").each(function() {
	    $(this)
	      .siblings(".panel-heading")
	      .find(".arrow-icon")
	      .addClass("rotate");
	  });

	  // Toggle plus minus icon on show hide of collapse element
	  $(".collapse")
	    .on("show.bs.collapse", function() {
	      $(this)
	        .parent()
	        .find(".arrow-icon")
	        .addClass("rotate");
	    })
	    .on("hide.bs.collapse", function() {
	      $(this)
	        .parent()
	        .find(".arrow-icon")
	        .removeClass("rotate");
	    });
	}

	return {
		init: function() {
			feather.replace();
			_accordionArrow();
		}
	}
})();

_appsJs.init();